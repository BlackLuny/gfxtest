#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include "gdiRender.h"

extern pluginHelper* helper;

extern "C" __declspec(dllexport)
basePlugin* createRender( pluginHelper* help, int version )
{
	if( PLUGINRENDER_VERSION != version ) {
		return NULL;
	}
	helper = help;
	return new gdiRender();
}

extern "C" __declspec(dllexport)
void destroyRender( basePlugin* render )
{
	delete render;
}

gdiRender::gdiRender()
	: mHdcBackBuffer( NULL ), mBitmap( NULL ), mBackBrush ( NULL ), mForeBrush( NULL )
{
	UseWMPaint = true;

	memset( mTriangle, 0, sizeof(mTriangle) );
}

gdiRender::~gdiRender()
{
}

bool gdiRender::preInit( WNDCLASSEX& wcex, RenderMode initialMode )
{
	wcex.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	return baseRender::preInit( wcex, initialMode );
}

bool gdiRender::init( HWND hwnd )
{
	mBackBrush = CreateSolidBrush( RGB( Background.r, Background.g, Background.b ) );
	mForeBrush = CreateSolidBrush( RGB( Object.r, Object.g, Object.b ) );
	mForePen = CreatePen( PS_NULL, 0, RGB(0,0,0) );

	this->mTriangle[0].x = Width / 2;
	this->mTriangle[0].y = Offset;
	this->mTriangle[1].x = Width - Offset;
	this->mTriangle[1].y = Height - Offset;
	this->mTriangle[2].x = Offset;
	this->mTriangle[2].y = Height - Offset;

	this->mHdcBackBuffer = CreateCompatibleDC(NULL);
	
	HDC dc = GetDC(hwnd);
	this->mBitmap = CreateCompatibleBitmap(dc, Width, Height);
	SelectObject(mHdcBackBuffer, mBitmap);
	ReleaseDC( hwnd, dc );
	return true;
}

void* gdiRender::modifyData( void* )
{
	return mHdcBackBuffer;
}

void gdiRender::frameTick( void* data, const float tick, float fps, wchar_t** gfxInfo, int mousePos[2] )
{
	HGDIOBJ hOrBrush = SelectObject( mHdcBackBuffer, mBackBrush );
	Rectangle( mHdcBackBuffer, 0, 0, Width, Height );
	SelectObject( mHdcBackBuffer, mForeBrush );
	HGDIOBJ hOrPen = SelectObject( mHdcBackBuffer, mForePen );
	Polygon( mHdcBackBuffer, mTriangle, 3 );
	SelectObject( mHdcBackBuffer, hOrPen );
	SelectObject( mHdcBackBuffer, hOrBrush );

	SetBkMode( mHdcBackBuffer, TRANSPARENT );
	SetTextColor( mHdcBackBuffer, RGB(255,255,255) );
	const wchar_t* buf = helper->va( L"fps: %5.02f", fps );
	RECT rc = {10, 10, 0, 0};
	rc.top += DrawTextW( mHdcBackBuffer, buf, -1, &rc, DT_NOCLIP );
	while( gfxInfo && *gfxInfo ) {
		rc.top += DrawTextW( mHdcBackBuffer, *gfxInfo, -1, &rc, DT_NOCLIP );
		++gfxInfo;
	}
	POINT cross[4] = { { mousePos[0], 0}, { mousePos[0], Height}, { 0, mousePos[1] }, { Width, mousePos[1] } };
	BYTE acts[4] = { PT_MOVETO, PT_LINETO, PT_MOVETO, PT_LINETO };
	PolyDraw( mHdcBackBuffer, cross, acts, 4 );
}

void gdiRender::flip( void* data )
{
	HDC hdc = static_cast<HDC>(data);
	BitBlt(hdc, 0, 0, Width, Height, mHdcBackBuffer, 0, 0, SRCCOPY);
}

void gdiRender::cleanup()
{
	DeleteObject( mBackBrush );
	DeleteObject( mForeBrush );
	DeleteObject( mForePen );

	DeleteObject( mBitmap );
	DeleteDC( mHdcBackBuffer );
}
