#ifndef GDIRENDER_H
#define GDIRENDER_H

#include "../baseRenderPlugin.h"


class gdiRender : public baseRender
{
public:
	gdiRender();
	virtual ~gdiRender();
	
	virtual bool isCapable( RenderMode mode ) const { return mode == render2D; }

	virtual bool preInit( WNDCLASSEX& wcex, RenderMode initialMode );
	virtual bool init( HWND hWnd );
	
	virtual void* modifyData( void* );
	virtual void frameTick( void* data, const float tick, float fps, wchar_t** gfxInfo, int mousePos[2] );
	virtual void flip( void* data );
	
	virtual void cleanup();

private:
	POINT mTriangle[3];
	HDC mHdcBackBuffer;
	HBITMAP mBitmap;
	HBRUSH mBackBrush;
	HBRUSH mForeBrush;
	HPEN mForePen;
};


#endif //#ifndef GDIRENDER_H