#include "winclude.h"
#include <WindowsX.h>
#include <Shlwapi.h>
#include "AutoLoadLibrary.h"

bool autoLoadLibs = false;
std::vector<AutoLoadLibrary> libsToAutoLoad;


void split(const char* str, const unsigned int length, const char* delim, std::vector<std::string>& outStr)
{
	std::string tmp;
	const char* iterator = str;
	for (; iterator < str + length; )
	{
		if (strncmp(iterator, delim, strlen(delim)) == 0)
		{
			if (!tmp.empty())
			{
				outStr.push_back(tmp);
				tmp.clear();
			}

			iterator += strlen(delim);
		}
		else
		{
			tmp += *iterator++;
		}
	}

	// Add the last line to the output vector.
	if (!tmp.empty())
	{
		outStr.push_back(tmp);
		tmp.clear();
	}
}

void WriteDefaultConfigurationFile(const wchar_t* path = L"GFXTest.conf")
{
	HANDLE hConfig = CreateFile(path, GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
	const char* contents = "#GFXTest configuration file\r\n#learn_more and evolution536\r\n\r\n#To auto-load libraries when GFXTest starts, set 'AutoLoadLibraries' to TRUE\r\n"\
		"#Add library paths to 'LibraryPath' fields (One path per line).\r\n#The libraries will be loaded in top-down order after the renderer and input plugins have been loaded.\r\n\r\n"\
		"AutoloadLibaries=FALSE\r\n#LibraryPath=C:\\TestCheat.dll";
	DWORD written;
	WriteFile(hConfig, contents, (DWORD)strlen(contents), &written, NULL);
	CloseHandle(hConfig);
}

const bool CheckInitConfigFile()
{
	HANDLE hConfig = CreateFile(L"GFXTest.conf", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hConfig != INVALID_HANDLE_VALUE)
	{
		LARGE_INTEGER fs;
		GetFileSizeEx(hConfig, &fs);
		
		// Idiots will always be idiots, so let's prevent GFXTest from crashing if the file is toyed around with.
		if (fs.LowPart > 512 * 1024 || fs.HighPart) // Bigger than 512kb
		{
			CloseHandle(hConfig);
			return false;
		}

		// Read configuration from the file.
		DWORD bytesRead;
		char* const buffer = new char[fs.LowPart + 1];
		ReadFile(hConfig, buffer, fs.LowPart, &bytesRead, NULL);
		buffer[fs.LowPart] = 0;
		std::vector<std::string> lines;
		split(buffer, strlen(buffer), "\r\n", lines);
		for (unsigned int i = 0; i < lines.size(); ++i)
		{
			// This line is a comment.
			if (lines[i][0] == '#')
			{
				continue;
			}

			// Parse fields.
			std::vector<std::string> lineFields;
			split(lines[i].c_str(), lines[i].length(), "=", lineFields);

			// If the autoload field is set and the variable is not yet set, we need to set this field.
			if (lineFields[0].compare("AutoloadLibaries") == 0 && !autoLoadLibs)
			{
				if (lineFields.size() > 1 && lineFields[1].compare("TRUE") == 0)
				{
					autoLoadLibs = true;
				}
			}
			// Let's load every library path we see.
			else if (lineFields[0].compare("LibraryPath") == 0)
			{
				if (lineFields.size() > 1)
				{
					AutoLoadLibrary lib;
					lib.BaseAddress = NULL;
					lib.LibraryPath = lineFields[1];
					libsToAutoLoad.push_back(lib);
				}
			}
		}
		delete[] buffer;
		CloseHandle(hConfig);
		return true;
	}

	// The configuration file may not exist yet. Let's create the default one.
	if (GetLastError() == ERROR_FILE_NOT_FOUND)
	{
		WriteDefaultConfigurationFile();
	}

	return false;
}

void InitAutoloadLibraries()
{
	// Read the configuration file, if it exists.
	if (!CheckInitConfigFile())
	{
		MessageBox(mainWnd, L"The configuration file could not be read. If the file did not exist, the default configuration file is now created. "\
			L"If changes are made in the configuration file, the application needs a restart.", L"Configuration Warning", MB_ICONEXCLAMATION);
	}

	// The renderer and input plugins should have been loaded by now. Let's load the configured autoload libraries.
	if (autoLoadLibs)
	{
		for (unsigned int i = 0; i < libsToAutoLoad.size(); ++i)
		{
			libsToAutoLoad[i].BaseAddress = LoadLibraryA(libsToAutoLoad[i].LibraryPath.c_str());
			if (libsToAutoLoad[i].BaseAddress)
			{
				std::wstring uistr;
				uistr.assign(libsToAutoLoad[i].LibraryPath.begin(), libsToAutoLoad[i].LibraryPath.end());
				ComboBox_AddString(hLoadedLibsCombo, PathFindFileName(uistr.c_str()));
			}
		}
	}
	else
	{
		Button_Enable(hButtonUnloadLib, FALSE);
		ComboBox_Enable(hLoadedLibsCombo, FALSE);
	}

	if (libsToAutoLoad.size() > 0)
	{
		ComboBox_SetCurSel(hLoadedLibsCombo, 0);
	}
}

