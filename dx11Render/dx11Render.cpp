#define WIN32_LEAN_AND_MEAN

#define _HAS_EXCEPTIONS 0

#include <Windows.h>
#include "dx11Render.h"
#include "FW1FontWrapper.h"

#include <d3d11.h>
#include <D3DCompiler.h>
#include <DirectXMath.h>

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

using namespace DirectX;

#if defined(NDEBUG) && !defined(_WIN64)
	#pragma comment(lib, "../msvcrt86.lib")
#endif

char vsHLSL[] = "struct PixelInputType { float4 position : SV_POSITION; float4 color : COLOR; }; struct VertexInputType { float4 position : POSITION; float4 color : COLOR; }; PixelInputType VS( VertexInputType input ) { PixelInputType output; output.position = input.position; output.color = input.color; return output; }";	
char psHLSL[] = "struct PixelInputType { float4 position : SV_POSITION; float4 color : COLOR; }; float4 PS( PixelInputType input ) : SV_TARGET { return input.color; }";

char vsHLSL3D[] = "cbuffer ConstantBuffer : register( b0 ) { matrix World; matrix View;	matrix Projection; } struct VS_OUTPUT { float4 Pos : SV_POSITION; float4 Color : COLOR0; }; VS_OUTPUT VS( float4 Pos : POSITION, float4 Color : COLOR ) { VS_OUTPUT output = (VS_OUTPUT)0; output.Pos = mul( Pos, World ); output.Pos = mul( output.Pos, View ); output.Pos = mul( output.Pos, Projection ); output.Color = Color; return output; }";
char psHLSL3D[] = "struct VS_OUTPUT { float4 Pos : SV_POSITION; float4 Color : COLOR0; }; float4 PS( VS_OUTPUT input ) : SV_Target { return input.Color; }";

extern pluginHelper* helper;

extern "C" __declspec(dllexport)
basePlugin* createRender( pluginHelper* help, int version )
{
	if( PLUGINRENDER_VERSION != version ) {
		return NULL;
	}
	helper = help;
	return new dx11Render();
}

extern "C" __declspec(dllexport)
void destroyRender( basePlugin* render )
{
	delete render;
}

struct MatrixBufferType
{
	XMMATRIX world;
	XMMATRIX view;
	XMMATRIX projection;
};

struct Vertex
{
	XMFLOAT3 pos;
	XMFLOAT4 color;
};

struct dx11Info
{
	ID3D11Device* d3dDevice;
	IDXGISwapChain* d3dSwapChain;
	ID3D11DeviceContext* d3dContext;
	ID3D11InputLayout* d3dInputLayout;
	ID3D11VertexShader* d3dVertexShader;
	ID3D11PixelShader* d3dPixelShader;
	ID3D11Buffer* d3dVertexBuffer;
	ID3D11Buffer* d3dIndexBuffer;
	ID3D11RenderTargetView* d3dRenderTarget;
	ID3D11Buffer* d3dMatrixConstantBuffer;
	ID3D11Buffer* d3dLineBuffer;

	IFW1FontWrapper* fw1Wrapper;
	IDWriteFactory* fw1DwriteFactory;
	IDWriteTextFormat* fw1TextFormat;

	XMMATRIX viewMatrix;
	XMMATRIX worldMatrix;
	XMMATRIX projectionMatrix;
	MatrixBufferType matrices3D;

	float ClearColor[4];
	float screenAspect;
};

void* originalHeapPtr = NULL;

const bool dx11Render::GFXTestSwitchModeTo2D()
{
	ID3D10Blob* d3dVertexShaderBuffer;
	ID3D10Blob* d3dPixelShaderBuffer;
	ID3D10Blob* err;

#ifdef _DEBUG
	if (FAILED(D3DCompile(vsHLSL, strlen(vsHLSL), NULL, NULL, NULL, "VS", "vs_4_0", D3DCOMPILE_SKIP_OPTIMIZATION, 0, &d3dVertexShaderBuffer, &err)) && err)
#else
	if (FAILED(D3DCompile(vsHLSL, strlen(vsHLSL), NULL, NULL, NULL, "VS", "vs_4_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &d3dVertexShaderBuffer, &err)) && err)
#endif
	{
		err->Release();
		return false;
	}

#ifdef _DEBUG
	if (FAILED(D3DCompile(psHLSL, strlen(psHLSL), NULL, NULL, NULL, "PS", "ps_4_0", D3DCOMPILE_SKIP_OPTIMIZATION, 0, &d3dPixelShaderBuffer, &err)) && err)
#else
	if (FAILED(D3DCompile(psHLSL, strlen(psHLSL), NULL, NULL, NULL, "PS", "ps_4_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &d3dPixelShaderBuffer, &err)) && err)
#endif
	{
		err->Release();
		return false;
	}

	D3D11_INPUT_ELEMENT_DESC inputElement[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	info->d3dDevice->CreateInputLayout(inputElement, ARRAYSIZE(inputElement), d3dVertexShaderBuffer->GetBufferPointer(), d3dVertexShaderBuffer->GetBufferSize(), &info->d3dInputLayout);
	info->d3dContext->IASetInputLayout(info->d3dInputLayout);

	info->d3dDevice->CreateVertexShader(d3dVertexShaderBuffer->GetBufferPointer(), d3dVertexShaderBuffer->GetBufferSize(), NULL, &info->d3dVertexShader);
	info->d3dDevice->CreatePixelShader(d3dPixelShaderBuffer->GetBufferPointer(), d3dPixelShaderBuffer->GetBufferSize(), NULL, &info->d3dPixelShader);

	d3dVertexShaderBuffer->Release();
	d3dPixelShaderBuffer->Release();

	info->d3dContext->VSSetShader(info->d3dVertexShader, NULL, 0);
	info->d3dContext->PSSetShader(info->d3dPixelShader, NULL, 0);

	Vertex vertexData[3];
	vertexData[0].pos = XMFLOAT3(0.0f, 0.7f, 0.0f);
	vertexData[1].pos = XMFLOAT3(0.7f, -0.7f, 0.0f);
	vertexData[2].pos = XMFLOAT3(-0.7f, -0.7f, 0.0f);

	// Set triangle color after dividing by 255 to fit the D3D11 color values.
	const float r11 = (float)Object.r / 0xFF;
	const float g11 = (float)Object.g / 0xFF;
	const float b11 = (float)Object.b / 0xFF;
	vertexData[0].color = XMFLOAT4(r11, g11, b11, 1.0f);
	vertexData[1].color = XMFLOAT4(r11, g11, b11, 1.0f);
	vertexData[2].color = XMFLOAT4(r11, g11, b11, 1.0f);

	D3D11_BUFFER_DESC desc;
	memset(&desc, 0, sizeof(D3D11_BUFFER_DESC));
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.ByteWidth = sizeof(Vertex) * 3;

	D3D11_SUBRESOURCE_DATA dataPointer;
	memset(&dataPointer, 0, sizeof(D3D11_SUBRESOURCE_DATA));
	dataPointer.pSysMem = vertexData;
	info->d3dDevice->CreateBuffer(&desc, &dataPointer, &info->d3dVertexBuffer);
	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	info->d3dContext->IASetVertexBuffers(0, 1, &info->d3dVertexBuffer, &stride, &offset);

	return true;
}

const bool dx11Render::GFXTestSwitchModeTo3D()
{
	ID3D10Blob* d3dVertexShaderBuffer;
	ID3D10Blob* d3dPixelShaderBuffer;
	ID3D10Blob* err;

#ifdef _DEBUG
	if (FAILED(D3DCompile(vsHLSL3D, strlen(vsHLSL3D), NULL, NULL, NULL, "VS", "vs_4_0", D3DCOMPILE_SKIP_OPTIMIZATION, 0, &d3dVertexShaderBuffer, &err)) && err)
#else
	if (FAILED(D3DCompile(vsHLSL3D, strlen(vsHLSL3D), NULL, NULL, NULL, "VS", "vs_4_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &d3dVertexShaderBuffer, &err)) && err)
#endif
	{
		err->Release();
		return false;
	}

#ifdef _DEBUG
	if (FAILED(D3DCompile(psHLSL3D, strlen(psHLSL3D), NULL, NULL, NULL, "PS", "ps_4_0", D3DCOMPILE_SKIP_OPTIMIZATION, 0, &d3dPixelShaderBuffer, &err)) && err)
#else
	if (FAILED(D3DCompile(psHLSL3D, strlen(psHLSL3D), NULL, NULL, NULL, "PS", "ps_4_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &d3dPixelShaderBuffer, &err)) && err)
#endif
	{
		err->Release();
		return false;
	}

	D3D11_INPUT_ELEMENT_DESC inputElement[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	info->d3dDevice->CreateInputLayout(inputElement, ARRAYSIZE(inputElement), d3dVertexShaderBuffer->GetBufferPointer(), d3dVertexShaderBuffer->GetBufferSize(), &info->d3dInputLayout);
	info->d3dContext->IASetInputLayout(info->d3dInputLayout);

	info->d3dDevice->CreateVertexShader(d3dVertexShaderBuffer->GetBufferPointer(), d3dVertexShaderBuffer->GetBufferSize(), NULL, &info->d3dVertexShader);
	info->d3dDevice->CreatePixelShader(d3dPixelShaderBuffer->GetBufferPointer(), d3dPixelShaderBuffer->GetBufferSize(), NULL, &info->d3dPixelShader);

	info->d3dContext->VSSetShader(info->d3dVertexShader, NULL, 0);
	info->d3dContext->PSSetShader(info->d3dPixelShader, NULL, 0);

	d3dVertexShaderBuffer->Release();
	d3dPixelShaderBuffer->Release();

	Vertex vertexData[] =
	{
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 1.0f, 1.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -0.9f, -1.0f), XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) },
	};

	D3D11_BUFFER_DESC desc;
	memset(&desc, 0, sizeof(D3D11_BUFFER_DESC));
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.ByteWidth = sizeof(Vertex) * 8;

	D3D11_SUBRESOURCE_DATA dataPointer;
	memset(&dataPointer, 0, sizeof(D3D11_SUBRESOURCE_DATA));
	dataPointer.pSysMem = vertexData;
	info->d3dDevice->CreateBuffer(&desc, &dataPointer, &info->d3dVertexBuffer);
	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	info->d3dContext->IASetVertexBuffers(0, 1, &info->d3dVertexBuffer, &stride, &offset);

	D3D11_BUFFER_DESC indexBufferDesc;
	memset(&indexBufferDesc, 0, sizeof(D3D11_BUFFER_DESC));
	indexBufferDesc.ByteWidth = sizeof(WORD) * 36;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	// 3D vertices box index array.
	WORD indices[] =
	{
		3, 1, 0,
		2, 1, 3,

		0, 5, 4,
		1, 5, 0,

		3, 4, 7,
		0, 4, 3,

		1, 6, 5,
		2, 6, 1,

		2, 7, 6,
		3, 7, 2,

		6, 4, 5,
		7, 4, 6,
	};

	D3D11_SUBRESOURCE_DATA indexData;
	memset(&indexData, 0, sizeof(D3D11_SUBRESOURCE_DATA));
	indexData.pSysMem = indices;
	info->d3dDevice->CreateBuffer(&indexBufferDesc, &indexData, &info->d3dIndexBuffer);

	info->d3dContext->IASetIndexBuffer(info->d3dIndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	D3D11_BUFFER_DESC matrixBufferDesc;
	memset(&matrixBufferDesc, 0, sizeof(D3D11_BUFFER_DESC));
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	info->d3dDevice->CreateBuffer(&matrixBufferDesc, NULL, &info->d3dMatrixConstantBuffer);

	// Create identities and basic 3D cube transformations.
	info->matrices3D.world = XMMatrixIdentity();
	info->matrices3D.projection = XMMatrixIdentity();
	info->matrices3D.view = XMMatrixIdentity();

	// Create matrices for 3D transformation.
	info->worldMatrix = XMMatrixIdentity();
	XMVECTOR eye = XMVectorSet(0.0f, 1.0f, -5.0f, 0.0f);
	XMVECTOR at = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	info->viewMatrix = XMMatrixLookAtLH(eye, at, up);
	info->projectionMatrix = XMMatrixPerspectiveFovLH(XM_PI / 2.5f, 1.0f, 0.01f, 100.0f);

	return true;
}

dx11Render::dx11Render()
{
	// Because we use vectorization (XMMATRIX) we need to align the info struct to a 16-byte boundary.
	originalHeapPtr = HeapAlloc(GetProcessHeap(), 0, sizeof(dx11Info) + 16);
	this->info = (dx11Info*)originalHeapPtr;
	AlignPointer((DWORD_PTR*)&this->info, 16);
	memset(this->info, 0, sizeof(dx11Info));
}

dx11Render::~dx11Render()
{
	HeapFree(GetProcessHeap(), 0, originalHeapPtr);
}

bool dx11Render::init( HWND hWnd )
{
	RECT rc;
	GetClientRect(hWnd, &rc);
	const UINT width = rc.right - rc.left;
	const UINT height = rc.bottom - rc.top;
	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)width;
	vp.Height = (FLOAT)height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;

	info->screenAspect = vp.Width / (FLOAT)vp.Height;

	info->ClearColor[0] = (float)Background.r / 0xFF;
	info->ClearColor[1] = (float)Background.g / 0xFF;
	info->ClearColor[2] = (float)Background.b / 0xFF;
	info->ClearColor[3] = 1.0f;

	D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	memset(&swapChainDesc, 0, sizeof(swapChainDesc));

	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = hWnd;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.Windowed = TRUE;
	swapChainDesc.BufferDesc.Height = height;
	swapChainDesc.BufferDesc.Width = width;

	UINT flags = 0;
	
	// Create D3D11 device and swapchain. These will be necessary all over the application.
	HRESULT h = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, flags, &featureLevel, 1
		, D3D11_SDK_VERSION, &swapChainDesc, &info->d3dSwapChain, &info->d3dDevice, NULL, &info->d3dContext);
	if (FAILED(h))
	{
		return false;
	}

	info->d3dContext->RSSetViewports(1, &vp);

	ID3D11Texture2D* d3dBackBuffer;
	info->d3dSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&d3dBackBuffer);
	info->d3dDevice->CreateRenderTargetView(d3dBackBuffer, NULL, &info->d3dRenderTarget);
	info->d3dContext->OMSetRenderTargets(1, &info->d3dRenderTarget, NULL);
	d3dBackBuffer->Release();

	info->d3dContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Create FW1FontWrapper to draw strings in D3D11.
	IFW1Factory* fac;
	FW1CreateFactory(FW1_VERSION, &fac);
	fac->CreateFontWrapper(info->d3dDevice, L"Microsoft Sans Serif", &info->fw1Wrapper);

	info->fw1Wrapper->GetDWriteFactory(&info->fw1DwriteFactory);
	info->fw1DwriteFactory->CreateTextFormat(L"Microsoft Sans Serif", NULL, DWRITE_FONT_WEIGHT_BOLD, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 16.0f, L"", &info->fw1TextFormat);
	
	fac->Release();

	if (ActiveMode == render2D)
	{
		return this->GFXTestSwitchModeTo2D();
	}

	return this->GFXTestSwitchModeTo3D();
}

void dx11Render::onChangeRenderMode()
{
	safeRelease( info->d3dVertexBuffer );
	safeRelease( info->d3dIndexBuffer );
	safeRelease( info->d3dVertexShader );
	safeRelease( info->d3dPixelShader );
	safeRelease( info->d3dMatrixConstantBuffer );
	safeRelease( info->d3dInputLayout );

	if (ActiveMode == render2D)
	{
		this->GFXTestSwitchModeTo2D();
	}
	else
	{
		this->GFXTestSwitchModeTo3D();
	}
}

void dx11Render::frameTick( void*, const float tick, float fps, wchar_t** gfxInfo, int mousePos[2] )
{
	info->d3dContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	info->d3dContext->ClearRenderTargetView(info->d3dRenderTarget, info->ClearColor);
	
	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	info->d3dContext->IASetVertexBuffers(0, 1, &info->d3dVertexBuffer, &stride, &offset);

	if (ActiveMode == render2D)
	{
		// Draw the triangle vertices.
		info->d3dContext->Draw(3, 0);
	}
	else
	{
		// Calculate the rotation for the 3D vertex box.
		XMMATRIX xRotation;
		info->worldMatrix = XMMatrixRotationY(tick / 400.0f);
		xRotation = XMMatrixRotationX(-0.5f);
		info->worldMatrix = XMMatrixMultiply(info->worldMatrix, xRotation);

		// Set matrices for 3D transformation.
		MatrixBufferType cb;
		cb.world = XMMatrixTranspose(info->worldMatrix);
		cb.view = XMMatrixTranspose(info->viewMatrix);
		cb.projection = XMMatrixTranspose(info->projectionMatrix);
		
		// Set 3D matrices as constant buffer and draw the indexed 3D box vertices.
		info->d3dContext->UpdateSubresource(info->d3dMatrixConstantBuffer, 0, NULL, &cb, 0, 0);
		info->d3dContext->VSSetConstantBuffers(0, 1, &info->d3dMatrixConstantBuffer);
		info->d3dContext->DrawIndexed(36, 0, 0);

		// Switch the world matrix to 2D identity.
		info->d3dContext->UpdateSubresource(info->d3dMatrixConstantBuffer, 0, NULL, &info->matrices3D, 0, 0);
		info->d3dContext->VSSetConstantBuffers(0, 1, &info->d3dMatrixConstantBuffer);
	}
	
	// Create text layout and FPS representation string and draw it.
	const wchar_t* buf = helper->va(L"fps: %5.02f", fps);
	IDWriteTextLayout* pTextLayout;
	DWRITE_LINE_METRICS met = {0};
	UINT actualLine = 0;
	info->fw1DwriteFactory->CreateTextLayout(buf, (UINT32)wcslen(buf), info->fw1TextFormat, 200.0f, 0.0f, &pTextLayout);
	pTextLayout->GetLineMetrics( &met, 1, &actualLine );
	float y = 10.f;
	info->fw1Wrapper->DrawTextLayout(info->d3dContext, pTextLayout, 10.0f, y, 0xFFFFFFFF, FW1_RESTORESTATE | FW1_NOWORDWRAP);
	pTextLayout->Release();
	y += met.height;

	// Draw the input string representations.
	while( gfxInfo && *gfxInfo )
	{
		info->fw1DwriteFactory->CreateTextLayout(*gfxInfo, (UINT32)wcslen(*gfxInfo), info->fw1TextFormat, 200.0f, 0.0f, &pTextLayout);
		info->fw1Wrapper->DrawTextLayout(info->d3dContext, pTextLayout, 10.0f, y, 0xFFFFFFFF, FW1_RESTORESTATE | FW1_NOWORDWRAP);
		y += met.height;
		++gfxInfo;
		pTextLayout->Release();
	}

	// D3D11 hands a left-handed coordinate system so the mouse position has to be converted to the usable range.
	const float px = (( 2.0f * (float)mousePos[0]) / (float)this->Width) - 1.0f;
	const float py = ((( 2.0f * (float)mousePos[1]) / (float)this->Height) - 1.0f) * -1.0f;

	// I tested this and apparently it runs faster when recreating the whole array every time.
	Vertex lines[] =
	{
		{ XMFLOAT3(-1.0f, py, 0.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, py, 0.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(px, -1.0f, 0.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(px, 1.0f, 0.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) },
	};

	// Create a buffer for the mouse position locator.
	D3D11_BUFFER_DESC desc;
	memset(&desc, 0, sizeof(D3D11_BUFFER_DESC));
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.ByteWidth = sizeof(Vertex) * sizeof(lines);

	D3D11_SUBRESOURCE_DATA dataPointer;
	memset(&dataPointer, 0, sizeof(D3D11_SUBRESOURCE_DATA));
	dataPointer.pSysMem = lines;
	info->d3dDevice->CreateBuffer(&desc, &dataPointer, &info->d3dLineBuffer);

	// Draw mouse position.
	info->d3dContext->IASetVertexBuffers(0, 1, &info->d3dLineBuffer, &stride, &offset);
	info->d3dContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	info->d3dContext->Draw(4, 0);

	info->d3dLineBuffer->Release();
	info->d3dLineBuffer = NULL;
}

void dx11Render::flip( void* )
{
	// Put first parameter as 1 to enable frame limiter, however, this fucks up the raw input processing...
	info->d3dSwapChain->Present(0, 0);
}

void dx11Render::cleanup()
{
	safeRelease(info->d3dLineBuffer);
	safeRelease( info->d3dInputLayout );
	safeRelease( info->d3dVertexShader );
	safeRelease( info->d3dPixelShader );
	safeRelease( info->d3dVertexBuffer );
	safeRelease( info->d3dIndexBuffer );
	safeRelease( info->d3dRenderTarget );
	safeRelease( info->d3dMatrixConstantBuffer );

	safeRelease(info->fw1DwriteFactory);
	safeRelease(info->fw1TextFormat);
	safeRelease(info->fw1Wrapper);
	safeRelease( info->d3dContext );
	safeRelease( info->d3dSwapChain );
	safeRelease( info->d3dDevice );
}

#ifdef NDEBUG
	// Linker screams for unresolved external since /NOENTRY is specified...
	void main () { }
#endif