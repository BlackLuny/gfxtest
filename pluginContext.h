template<typename T>
struct pluginContext
{
	std::vector<plugin*> all;		//yes this as pointer sucks, see main.cpp for the reason...
	plugin* next;
	plugin* active;
	T* ptr;

	pluginContext() : next(NULL), active(NULL), ptr(NULL) {;}

	//dirty little tricks, the ptr->init resolves on compile time, so because the 2 init functions are called the same, this actually works...
	bool init( HWND hWnd )
	{
		if( !ptr->init( hWnd ) ) {
			active->markAsFailed();
			return false;
		}
		return true;
	}
	void unload()
	{
		if( ptr && active ) {
			active->unload( ptr );
			ptr = NULL;
		}
	}
	void advance()
	{
		if( next ) {
			active = next;
			next = NULL;
		}
	}
	bool advanceAfterFailure()
	{
		if( !next || next == active ) {
			for( size_t j = 0; j < all.size(); ++j ){
				plugin* plug = all[j];
				if( !plug->isFailed() ) {
					next = plug;
					break;
				}
			}
			if( !next || next == active ) {
				return false;
			}
		}
		return true;
	}
};
