#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include "rawInput.h"

extern baseInputHelper* helper;

extern "C" __declspec(dllexport)
basePlugin* createInput( baseInputHelper* help, int version )
{
	if( PLUGININPUT_VERSION != version ) {
		return NULL;
	}
	helper = help;
	return new rawInput();
}

extern "C" __declspec(dllexport)
void destroyInput( basePlugin* render )
{
	delete render;
}

struct wmInfo
{
	HWND hwnd;
	bool Registered[baseInput::MAX_TYPE];
};

rawInput::rawInput()
:info(*(new wmInfo()))
{
	memset( &info, 0, sizeof(wmInfo) );
}

rawInput::~rawInput()
{
	delete &info;
}

bool rawInput::init( HWND hWnd )
{
	info.hwnd = hWnd;

	for( size_t n = 0; n < baseInput::MAX_TYPE; ++n ) {
		if( Capture[n] ) {
			if( !changeDeviceRegistration( (baseInput::Type)n, 0 ) ) {
				return false;
			}
		}
	}
	return true;
}

LRESULT rawInput::processMessage( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam, int& blockDef )
{
	if( message == WM_INPUT ) {
		UINT dwSize = 100;
		PBYTE data[100];
		UINT r = GetRawInputData( reinterpret_cast<HRAWINPUT>(lParam), RID_INPUT, data, &dwSize, sizeof(RAWINPUTHEADER) );
		if( r != 0xffffffff ) {
			RAWINPUT* raw = reinterpret_cast<RAWINPUT*>(data);
			const wchar_t* buf = NULL;
			if( RIM_TYPEKEYBOARD == raw->header.dwType ) {
				RAWKEYBOARD& keyboard = raw->data.keyboard;
				helper->keyEvent(((keyboard.MakeCode&0xff) << 16) | ((keyboard.Flags & RI_KEY_E0) ? 0x1000000 : 0), keyboard.Message == WM_KEYDOWN || keyboard.Message == WM_SYSKEYDOWN);
			} else if( RIM_TYPEMOUSE == raw->header.dwType ) {
				RAWMOUSE& mouse = raw->data.mouse;
				if( mouse.lLastX || mouse.lLastY || (mouse.usFlags & MOUSE_MOVE_ABSOLUTE) ) {
					if( mouse.usFlags & MOUSE_MOVE_ABSOLUTE ) {
						helper->mouseMoveTo( mouse.lLastX, mouse.lLastY );
					} else {
						helper->mouseMoveAdd( mouse.lLastX, mouse.lLastY );
					}
				}
				for( int n = 0; n < 10; ++n ) {
					if( mouse.usButtonFlags & (1<<n) ) {
						helper->mouseButton( (n >> 1) + mbLeft, !(n % 2) );
					}
				}
			} else {
				buf = helper->va( L"Unknown type: %d", raw->header.dwType );
			}
			//OutputDebugStringA( buf );
		}
		//delete[] data;
		//blockDef = 1;
	}
	return 0;
}

#ifndef HID_USAGE_PAGE_GENERIC
#define HID_USAGE_PAGE_GENERIC         ((USHORT) 0x01)
#endif
#ifndef HID_USAGE_GENERIC_MOUSE
#define HID_USAGE_GENERIC_MOUSE        ((USHORT) 0x02)
#endif

bool rawInput::changeDeviceRegistration( baseInput::Type type, DWORD flags )
{
	RAWINPUTDEVICE Rid = {0};
	Rid.usUsagePage = HID_USAGE_PAGE_GENERIC;
	Rid.usUsage = type == baseInput::Mouse ? HID_USAGE_GENERIC_MOUSE : 0x06;
	Rid.dwFlags = /*RIDEV_NOLEGACY |*/ flags;
	Rid.hwndTarget = (flags & RIDEV_REMOVE) ? 0 : info.hwnd;
	return info.Registered[type] = RegisterRawInputDevices( &Rid, 1, sizeof(Rid) ) != FALSE;
}


bool rawInput::isCapable( Type type ) const
{
	return true;	//check for devices available?
}

void rawInput::onChangeCapture( baseInput::Type type )
{
	if( info.Registered[type] != Capture[type] ) {
		changeDeviceRegistration( type, Capture[type] ? 0 : RIDEV_REMOVE );
	}
}


void rawInput::cleanup()
{
	for( size_t n = 0; n < baseInput::MAX_TYPE; ++n ) {
		if( info.Registered[n] ) {
			changeDeviceRegistration( (baseInput::Type)n, RIDEV_REMOVE );
		}
	}
}
