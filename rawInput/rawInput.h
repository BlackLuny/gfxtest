#ifndef RAWINPUT_H
#define RAWINPUT_H

#include "../baseInputPlugin.h"

struct wmInfo;

class rawInput : public baseInput
{
public:
	rawInput();
	virtual ~rawInput();

	virtual bool init( HWND hWnd );
	virtual LRESULT processMessage( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam, int& blockDef );

	virtual bool isCapable( Type type ) const;
	virtual void onChangeCapture( Type type );

	virtual void cleanup();

	bool changeDeviceRegistration( baseInput::Type type, DWORD flags );

private:
	rawInput& operator = ( const rawInput& );
	wmInfo& info;
};


#endif //#ifndef RAWINPUT_H
