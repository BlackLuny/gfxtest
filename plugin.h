#pragma once

#include <vector>
#include "basePlugin.h"
#include "gfxtestPlugin.h"

class pluginBase
{
public:
	enum Type {
		Renderer,
		Input,
		Test
	};

	virtual ~pluginBase();

	virtual bool load();
	virtual void unload();

	inline const std::wstring& name() const { return mName; }
	inline Type type() const { return mType; }
	inline HMODULE ptr() const { return mModule; }

protected:
	template<class T>
	static void buildPluginList( const std::wstring& appPath, std::vector<T*>& plugins, Type type, T* (*loadProc)(const std::wstring&,const std::wstring&,Type) );

	pluginBase( const std::wstring& name, const std::wstring& path, Type type );

	std::wstring mName;
	std::wstring mFile;
	Type mType;
	HMODULE mModule;
};

class plugin : public pluginBase
{
public:
	inline bool isFailed() const { return mFailed; }
	void markAsFailed() { mFailed = true; }


	basePlugin* load( int version );
	void unload( basePlugin* item );


	static void getPlugins( const std::wstring& appPath, std::vector<plugin*>& plugins, pluginBase::Type type );

protected:
	void callDestroy( basePlugin* item );


	bool mFailed;

private:
	plugin( const std::wstring& name, const std::wstring& path, pluginBase::Type type );

	static plugin* createPluginProc( const std::wstring& fullPath, const std::wstring& fileName, Type type );
};


class testPlugin : public pluginBase
{
public:

	virtual bool load();
	virtual void unload();

	void callback( int pluginEvent, void* arg );


	static void getPlugins( const std::wstring& appPath, std::vector<testPlugin*>& plugins );
protected:
	pluginCallbackProc mCallback;

private:
	testPlugin( const std::wstring& name, const std::wstring& path, HMODULE hMod );
	static testPlugin* createTestPluginProc( const std::wstring& fullPath, const std::wstring& fileName, Type );
};
