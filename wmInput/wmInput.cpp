#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include "wmInput.h"

extern baseInputHelper* helper;

extern "C" __declspec(dllexport)
basePlugin* createInput( baseInputHelper* help, int version )
{
	if( PLUGININPUT_VERSION != version ) {
		return NULL;
	}
	helper = help;
	return new wmInput();
}

extern "C" __declspec(dllexport)
void destroyInput( basePlugin* render )
{
	delete render;
}

struct wmInfo
{
	HWND hwnd;
};

wmInput::wmInput()
:info(*(new wmInfo()))
{
	memset( &info, 0, sizeof(wmInfo) );
}

wmInput::~wmInput()
{
	delete &info;
}

bool wmInput::init( HWND hWnd )
{
	info.hwnd = hWnd;
	return true;
}

#define CHECK_BUTTONS(down, up, send ) \
	case down: case up: if( Capture[Mouse] ) { helper->mouseButton( send, down == message ); return 0; } break;

LRESULT wmInput::processMessage( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam, int& blockDef )
{
	switch( message ) {
		case WM_KEYUP:
		case WM_KEYDOWN:
		case WM_SYSKEYUP:
		case WM_SYSKEYDOWN:
			if( Capture[Keyboard] ) {
				helper->keyEvent( lParam & 0x1ff0000, message == WM_KEYDOWN || message == WM_SYSKEYDOWN );
				return 0;
			}
			break;
		case WM_MOUSEMOVE:
			if( Capture[Mouse] ) {
				helper->mouseMoveTo( LOWORD(lParam), HIWORD(lParam) );
				return 0;
			}
			break;
		CHECK_BUTTONS(WM_LBUTTONDOWN, WM_LBUTTONUP, mbLeft );
		CHECK_BUTTONS(WM_RBUTTONDOWN, WM_RBUTTONUP, mbRight );
		CHECK_BUTTONS(WM_MBUTTONDOWN, WM_MBUTTONUP, mbMiddle );
		case WM_XBUTTONDOWN:
		case WM_XBUTTONUP:
			if( Capture[Mouse] ) {
				helper->mouseButton( (HIWORD(wParam) == XBUTTON1) ? mbButton4 : mbButton5, WM_XBUTTONDOWN == message );
				return 0;
			}
			break;
		default:
			break;
	}
	return 0;
}

bool wmInput::isCapable( Type type ) const
{
	return true;
}

void wmInput::cleanup()
{
}
