#ifndef WMINPUT_H
#define WMINPUT_H

#include "../baseInputPlugin.h"

struct wmInfo;

class wmInput : public baseInput
{
public:
	wmInput();
	virtual ~wmInput();

	virtual bool init( HWND hWnd );
	virtual LRESULT processMessage( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam, int& blockDef );
	
	virtual bool isCapable( Type type ) const;

	virtual void cleanup();

private:
	wmInput& operator = ( const wmInput& );
	wmInfo& info;
};


#endif //#ifndef WMINPUT_H
