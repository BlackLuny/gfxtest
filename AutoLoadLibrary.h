#ifndef AUTOLOADLIBRARY_H
#define AUTOLOADLIBRARY_H

#include <string>
#include <vector>

struct AutoLoadLibrary
{
	HMODULE BaseAddress;
	std::string LibraryPath;
};

// exposed by AutoLoadLibrary
extern std::vector<AutoLoadLibrary> libsToAutoLoad;

// exposed by main
extern HWND mainWnd;
extern HWND hLoadedLibsCombo;
extern HWND hButtonUnloadLib;

void InitAutoloadLibraries();

#endif // AUTOLOADLIBRARY_H
