#include "winclude.h"
#include <WindowsX.h>
#include <ShellAPI.h>
#include <Commdlg.h>	//ChooseColor
#include <tchar.h>
#include "plugin.h"
#include "resource.h"
#include "simpleFPS.h"
#include "baseRenderPlugin.h"
#include "baseInputPlugin.h"
#include "AutoLoadLibrary.h"


#pragma comment(lib, "shlwapi.lib")

#include <commctrl.h>
#pragma comment( lib, "comctl32" )
#pragma comment( linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"" )

// http://molecularmusings.wordpress.com/2011/09/05/properly-handling-keyboard-input/

#include "pluginContext.h"

// Plugin globals

std::wstring appPath;

pluginContext<baseRender> renderer;
pluginContext<baseInput> input;

std::vector<testPlugin*> notifyPluginList;

// Helper class globals

simpleFPS fps;

// Process instance globals

HMENU hPopupMenu = NULL;
HINSTANCE hInstance = NULL;
bool keepRunning = true;

// Window globals

LONG dragTop = 0;
LONG dragLeft = 0;

DWORD ResolutionSelection = 0;
baseRender::RenderMode initialMode;

HWND mainWnd = NULL;
HWND settingWnd = NULL;
HWND hComboRender = NULL;
HWND hComboInput = NULL;
HWND hComboResolution = NULL;
HWND hLoadedLibsCombo = NULL;
HWND hButtonUnloadLib = NULL;

struct ColorHelper
{
	Color col;
	HWND ctrl;
	HBRUSH brush;

	void update()
	{
		if( brush ) {
			DeleteObject( brush );
		}
		brush = CreateSolidBrush( RGB( col.r, col.g, col.b ) );
	}
};
ColorHelper colTriangle = { Color(255, 0, 0), NULL, NULL };
ColorHelper colBackground = { Color(255, 0, 255), NULL, NULL };

// Resolution selection lookup table.
// now that we have a config file, we can add custom resolutions.
ResolutionDesc resoTable[] =
{
	{ 640, 480 },
	{ 800, 600 },
	{ 1024, 768 },
	{ 1152, 864 },
	{ 1280, 720 },
	{ 1280, 768 },
	{ 1440, 900 },
	{ 1600, 900 },
	{ 1680, 1050 },
	{ 1920, 1080 }
};

size_t resoTableSize = _countof(resoTable);

//input helper functions:
void i_init( int screenSize[2] );
wchar_t** i_frame( float tick, int mousePos[2] );

// Functions
// --------------------------------------------------------------------------------------------------------------

void updateMenu( HMENU menu, plugin* act )
{
	CheckMenuItem( menu, (UINT)act, (renderer.active == act || input.active == act) ? MF_CHECKED : MF_UNCHECKED );
}

void updateAllMenu( HMENU menu, std::vector<plugin*>& all )
{
	for( size_t n = 0; n < all.size(); ++n ) {
		updateMenu( menu, all[n] );
	}
}

void notifyPlugins( int pluginEvent, void* arg )
{
	for( size_t n = 0; n < notifyPluginList.size(); ++n ) {
		notifyPluginList[n]->callback( pluginEvent, arg );
	}
}

HMENU getPopup()
{
	updateAllMenu( hPopupMenu, renderer.all );
	updateAllMenu( hPopupMenu, input.all );
	return hPopupMenu;
}

void onInit( HWND hDlg )
{
	hComboRender = GetDlgItem( hDlg, IDC_RENDERER );
	hComboInput = GetDlgItem( hDlg, IDC_INPUT );
	hComboResolution = GetDlgItem(hDlg, IDC_RESOLUTION);
	hLoadedLibsCombo = GetDlgItem(hDlg, IDC_LOADEDLIBSDROP);
	hButtonUnloadLib = GetDlgItem(hDlg, IDC_UNLOADLIB);

	colTriangle.ctrl = GetDlgItem(hDlg, IDC_COL_TRI);
	colTriangle.update();
	colBackground.ctrl = GetDlgItem(hDlg, IDC_COL_BG);
	colBackground.update();

	//"GFXTest by learn_more and evolution536"
	DWORD dwTxt[] = {0x460047, 0x540058, 0x730065, 0x200074, 0x790062, 0x6c0020, 0x610065, 0x6e0072, 0x6d005f, 0x72006f, 0x200065, 0x6e0061, 0x200064, 0x760065, 0x6c006f, 0x740075, 0x6f0069, 0x35006e, 0x360033, 0x0};
	Static_SetText(GetDlgItem(hDlg, IDC_CREDITS), (wchar_t*)dwTxt );

	for( size_t n = 0; n < renderer.all.size(); ++n ) {
		const plugin* p = renderer.all[n];
		if( p->isFailed() ) {
			continue;	//don't add to the combobox...
		}
		const size_t idx = ComboBox_AddString( hComboRender, p->name().c_str() );
		ComboBox_SetItemData( hComboRender, idx, p );
		if( p == renderer.active ) {
			ComboBox_SetCurSel( hComboRender, idx );
			EnableWindow( GetDlgItem(hDlg,IDC_RENDER2D), renderer.ptr->isCapable(baseRender::render2D) ? 1 : 0 );
			EnableWindow( GetDlgItem(hDlg,IDC_RENDER3D), renderer.ptr->isCapable(baseRender::render3D) ? 1 : 0 );
			if( renderer.ptr->ActiveMode == baseRender::render2D ) {
				CheckDlgButton( hDlg, IDC_RENDER2D, BST_CHECKED );
			} else {
				CheckDlgButton( hDlg, IDC_RENDER3D, BST_CHECKED );
			}
		}
	}
	for( size_t n = 0; n < input.all.size(); ++n ) {
		const plugin* p = input.all[n];
		if( p->isFailed() ) {
			continue;	//don't add to the combobox...
		}
		const size_t idx = ComboBox_AddString( hComboInput, p->name().c_str() );
		ComboBox_SetItemData( hComboInput, idx, p );
		if( p == input.active ) {
			ComboBox_SetCurSel( hComboInput, idx );
			bool m = input.ptr->isCapable(baseInput::Mouse);
			bool k = input.ptr->isCapable(baseInput::Keyboard);
			EnableWindow( GetDlgItem(hDlg,IDC_MOUSE), m ? 1 : 0 );
			EnableWindow( GetDlgItem(hDlg,IDC_KEYBOARD), k ? 1 : 0 );
			CheckDlgButton( hDlg, IDC_MOUSE, (m && input.ptr->Capture[baseInput::Mouse]) ? BST_CHECKED : BST_UNCHECKED );
			CheckDlgButton( hDlg, IDC_KEYBOARD, (k && input.ptr->Capture[baseInput::Keyboard]) ? BST_CHECKED : BST_UNCHECKED );
		}
	}

	for( size_t n = 0; n < resoTableSize; ++n ) {
		wchar_t buf[50];
		swprintf_s( buf, L"%d x %d", resoTable[n].w, resoTable[n].h );
		ComboBox_AddString(hComboResolution, buf);
	}
	ComboBox_SetCurSel(hComboResolution, ResolutionSelection);
}

void destroyMainWindow()
{
	// When the window is destroyed, the libs are reloaded. Free them before destroying the window.
	for (unsigned int i = 0; i < libsToAutoLoad.size(); ++i)
	{
		FreeLibrary(libsToAutoLoad[i].BaseAddress);
	}

	notifyPlugins( pe_DestroyWindow, mainWnd );
	DestroyWindow( mainWnd );
}

void updateNextFromCombo( HWND hCombo, plugin* &next )
{
	int sel = ComboBox_GetCurSel( hCombo );
	if( sel != CB_ERR ) {
		if( plugin* p = (plugin*)ComboBox_GetItemData( hCombo, sel ) ) {
			next = p;
			destroyMainWindow();
		}
	}
}

void handleColor( ColorHelper& helper )
{
	CHOOSECOLOR cc = { sizeof(cc) };
	static COLORREF acrCustClr[16] = {0};
	cc.hwndOwner = mainWnd;
	cc.Flags = CC_RGBINIT | CC_ANYCOLOR | CC_FULLOPEN;
	cc.rgbResult = RGB( helper.col.r, helper.col.g, helper.col.b );
	cc.lpCustColors = (LPDWORD) acrCustClr;
	if( ChooseColorW( &cc ) ) {
		helper.col.r = GetRValue(cc.rgbResult);
		helper.col.g = GetGValue(cc.rgbResult);
		helper.col.b = GetBValue(cc.rgbResult);
		helper.update();
		InvalidateRect( helper.ctrl, NULL, FALSE );
		destroyMainWindow();
	}
}

INT_PTR CALLBACK settingDialogProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	switch( uMsg ) {
		case WM_INITDIALOG:
			onInit( hDlg );
			break;
		case WM_COMMAND:
			if( HIWORD(wParam) == CBN_SELENDOK ) {
				if( lParam == (LPARAM)hComboRender )
				{
					updateNextFromCombo( hComboRender, renderer.next );
				}
				else if( lParam == (LPARAM)hComboInput )
				{
					updateNextFromCombo( hComboInput, input.next );
				}
				else if (lParam == (LPARAM)hComboResolution)
				{
					ResolutionSelection = ComboBox_GetCurSel(hComboResolution);
					if( ResolutionSelection < 0 || ResolutionSelection >= resoTableSize ) {
						ResolutionSelection = 0;
					}
					destroyMainWindow();
				}
			} else if( HIWORD(wParam) == BN_CLICKED ) {
				if( LOWORD(wParam) == IDC_RENDER2D || LOWORD(wParam) == IDC_RENDER3D ) {
					const baseRender::RenderMode tempMode = IsDlgButtonChecked(hDlg, IDC_RENDER3D) ? baseRender::render3D : baseRender::render2D;
					renderer.ptr->setRenderMode(tempMode);
					initialMode = tempMode;
				} else if( LOWORD(wParam) == IDC_COL_TRI ) {
					handleColor( colTriangle );
				} else if( LOWORD(wParam) == IDC_COL_BG ) {
					handleColor( colBackground );
				} else if( LOWORD(wParam) == IDC_KEYBOARD ) {
					input.ptr->setCapture( baseInput::Keyboard, IsDlgButtonChecked(hDlg, IDC_KEYBOARD) != 0 );
				} else if( LOWORD(wParam) == IDC_MOUSE ) {
					input.ptr->setCapture( baseInput::Mouse, IsDlgButtonChecked(hDlg, IDC_MOUSE) != 0 );
				}
				else if (LOWORD(wParam) == IDC_UNLOADLIB)
				{
					const unsigned int sel = ComboBox_GetCurSel(hLoadedLibsCombo);
					if (sel >= 0 && sel < libsToAutoLoad.size())
					{
						// Temporary but ugly solution: FreeLibrary succession does not guarantee that the module is unloaded.
						// Therefore I decided to check whether information about the plugin can still be found.
						wchar_t tempstring[MAX_PATH];
						const BOOL b = FreeLibrary(libsToAutoLoad[sel].BaseAddress);

						if (b && !GetModuleFileName(libsToAutoLoad[sel].BaseAddress, tempstring, MAX_PATH))
						{
							libsToAutoLoad.erase(libsToAutoLoad.begin() + sel);
							ComboBox_DeleteString(hLoadedLibsCombo, sel);
						}
						else
						{
							MessageBox(mainWnd, L"The library was not succesfully unloaded!", L"Library Error", MB_ICONERROR);
						}
					}
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			if( reinterpret_cast<HWND>(lParam) == colTriangle.ctrl ) {
				return reinterpret_cast<INT_PTR>(colTriangle.brush);
			} else if( reinterpret_cast<HWND>(lParam) == colBackground.ctrl ) {
				return reinterpret_cast<INT_PTR>(colBackground.brush);
			}
			break;
		case WM_DESTROY:
			hComboRender = 0;
			break;
	}
	return FALSE;	//have dlg manager process it.
}

void callRender( HDC hdc )
{
	fps.frame();
	void* pluginArg = renderer.ptr->modifyData( hdc );	//we give the plugin the chance to modify this (f.e. change into dx device ptr)

	int mousePos[2] = {0};

	wchar_t** gfxInfo = i_frame( fps.tick(), mousePos );

	notifyPlugins( pe_PreRender, pluginArg );
	renderer.ptr->frameTick( hdc, fps.tick(), fps.fps(), gfxInfo, mousePos );	//but, we do not give them their own modified data..
	notifyPlugins( pe_PostRender, pluginArg );
	renderer.ptr->flip( hdc );
}

//allows plugins to suppress escape key.
void escapePressed()
{
	destroyMainWindow();
	keepRunning = false;
}

LRESULT CALLBACK mainWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch( message ) {
	case WM_CREATE:
		settingWnd = CreateDialog( hInstance, MAKEINTRESOURCE(IDD_OPTIONS), hWnd, settingDialogProc );
		break;
	case WM_PAINT:
		if( renderer.ptr->UseWMPaint ) {
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint( hWnd, &ps );
			callRender( hdc );
			EndPaint(hWnd, &ps);
		}
		break;
	case WM_SYSCOMMAND:
		if( SC_CLOSE == wParam ) {
			keepRunning = false;
		}
		break;
	case WM_CONTEXTMENU:
		if( SetForegroundWindow( hWnd ) ) {
			POINT pt = { LOWORD(lParam), HIWORD(lParam) };
			POINT pt2 = pt;
			RECT rc;
			GetClientRect( hWnd, &rc );
			ScreenToClient( hWnd, &pt2 );
			if( PtInRect( &rc, pt2 ) ) {
				int align = GetSystemMetrics( SM_MENUDROPALIGNMENT );
				plugin* p = (plugin*)TrackPopupMenu( getPopup(), TPM_RETURNCMD | align, pt.x, pt.y, 0, hWnd, NULL );
				PostMessage( hWnd, WM_NULL, 0, 0 );
				if( !p || (plugin*)1 == p ) {
					if( p ) {
						keepRunning = false;
						destroyMainWindow();
					}
				} else {
					if( plugin::Renderer == p->type() ) {
						renderer.next = p;
						destroyMainWindow();
					} else if( plugin::Input == p->type() ) {
						input.next = p;
						destroyMainWindow();
					}
				}
			}
		}
		break;
	case WM_DESTROY:
		destroyMainWindow();
		settingWnd = 0;
		PostQuitMessage( 0 );
		break;
	case WM_MOVE:
		if( IsWindow(settingWnd) ) {
			int xPos = (int)(short) LOWORD(lParam);
			int yPos = (int)(short) HIWORD(lParam);
			RECT rc;
			GetWindowRect( settingWnd, &rc );
			MoveWindow( settingWnd, xPos + dragLeft, yPos + dragTop, rc.right-rc.left, rc.bottom-rc.top, FALSE );
		}
		break;
	default:
		break;
	}

	if( input.ptr ) {
		int blockDef = 0;
		LRESULT lRes = input.ptr->processMessage( hWnd, message, wParam, lParam, blockDef );
		if( blockDef ) {
			return lRes;
		}
	}
	return DefWindowProc( hWnd, message, wParam, lParam );
}

ATOM registerClass( const TCHAR* className, baseRender::RenderMode initialMode )
{
	WNDCLASSEX wcex = { sizeof(wcex), NULL };
	wcex.lpszClassName = className;
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = mainWndProc;
	wcex.hInstance = hInstance;
	wcex.hCursor = LoadCursor( NULL, IDC_ARROW );
	if( !renderer.ptr->preInit( wcex, initialMode ) ) {
		wchar_t err_msg[256];
		swprintf_s(err_msg, L"The selected renderer '%s' is not capable of providing the selected rendering mode (%cD).",
			renderer.active->name().c_str(), initialMode == baseRender::render2D ? L'2' : L'3');
		MessageBox( NULL, err_msg, TEXT("GFXTest Error"), MB_ICONERROR );
		return 0;
	}
	return RegisterClassEx( &wcex );
}

bool createWindowAndRun( baseRender::RenderMode initialMode )
{
	// moved to here, since it is not necessary as global variable and it increases image size if it is declared so?
	// lm: doesnt matter, the text is in .rdata anyway, and one pointer more or less.
	//	the padding in any of the sections is around 0x200, so unless that 4 bytes would be just below the boundary and add another 0x200 it doesnt change a thing.
	const TCHAR* className = TEXT("gfx_test");

	bool exitOk = true;
	renderer.ptr = static_cast<baseRender*>(renderer.active->load( PLUGINRENDER_VERSION ));
	if( !renderer.ptr ) {
		MessageBox( NULL, TEXT("The renderer failed to load because the library did not load succesfully."), TEXT("GFXTest Error"), MB_ICONERROR );
		renderer.active->markAsFailed();
		return false;
	}
	const ResolutionDesc* resoSel = &resoTable[ResolutionSelection];
	renderer.ptr->Width = resoSel->w;
	renderer.ptr->Height = resoSel->h;
	renderer.ptr->Object = colTriangle.col;
	renderer.ptr->Background = colBackground.col;
	int tmp[2] = { resoSel->w, resoSel->h };
	i_init( tmp );
	notifyPlugins( pe_LoadRender, renderer.active->ptr() );
	input.ptr = static_cast<baseInput*>(input.active->load( PLUGININPUT_VERSION ));
	if( input.ptr ) {
		notifyPlugins( pe_LoadInput, input.active->ptr() );
		ATOM wndClass = registerClass( className, initialMode );
		if( wndClass ) {
			DWORD dwStyle = WS_POPUPWINDOW | WS_CAPTION;
			DWORD dwExStyle = 0;
			RECT rc = { 0, 0, renderer.ptr->Width, renderer.ptr->Height };
			AdjustWindowRectEx( &rc, dwStyle, FALSE, dwExStyle );
			dragTop = 0;
			dragLeft = rc.right - rc.left;
			RECT work;
			SystemParametersInfo( SPI_GETWORKAREA, 0, &work, 0 );
			long ww = rc.right - rc.left;
			long wh = rc.bottom - rc.top;
			long wx = ((work.right-work.left-ww) >> 1) + work.left;
			long wy = ((work.bottom-work.top-wh) >> 1) + work.top;

			// This prevents the window being placed outside of the users reach. If it is not applied, the user may not be able to close the
			// window on systems with a lower desktop screen resolution.
			if (wy < 0)
			{
				wy = 0;
			}

			fps.init();
			{
				wchar_t buf[512];
				swprintf_s(buf, L"Renderer: [%s], Input: [%s], " PLUGIN_ARCH L" bits", renderer.active->name().c_str(), input.active->name().c_str());
				mainWnd = CreateWindowEx(dwExStyle, className, buf, dwStyle, wx, wy, ww, wh, NULL, NULL, hInstance, NULL);
			}

			notifyPlugins( pe_CreateWindow, mainWnd );

			if( renderer.init( mainWnd ) && input.init( mainWnd ) )
			{
				ShowWindow( mainWnd, SW_SHOW );
				InitAutoloadLibraries();

				MSG msg = { NULL };
				bool bRun = true;
				while( bRun ) {
					if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) ) {
						if( !IsWindow(settingWnd) || !IsDialogMessage(settingWnd, &msg) ) {
							TranslateMessage( &msg );
							DispatchMessage( &msg );
						}
						if( msg.message == WM_QUIT ) {
							bRun = false;
						}
					}
					if( !renderer.ptr->UseWMPaint ) {
						callRender( NULL );
					} else {
						InvalidateRect( mainWnd, NULL, FALSE );	//force a redraw in gdi
					}
				}
			} else {
				if( renderer.active->isFailed() ) {
					MessageBox( NULL, TEXT("The renderer failed to initialize. Are you sure your graphics card supports the renderer?"), TEXT("GFXTest Error"), MB_ICONERROR );
				} else {
					MessageBox( NULL, TEXT("The input library failed to initialize."), TEXT("GFXTest Error"), MB_ICONERROR );
				}
				exitOk = false;
				destroyMainWindow();	//without us destroying the window we cannot un-register the class.
				MSG msg = { NULL };
				//make sure to eat the wm_quit message so it does not end up in our next loop...
				while( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) ) {
					if( msg.message == WM_QUIT ) {
						break;
					}
				}
			}
			renderer.ptr->cleanup();
			input.ptr->cleanup();
			UnregisterClass( className, hInstance );
			mainWnd = 0;
		}
	} else {
		MessageBox( NULL, TEXT("The input failed to load because the library did not load succesfully."), TEXT("GFXTest Error"), MB_ICONERROR );
		input.active->markAsFailed();
		exitOk = false;
	}
	notifyPlugins( pe_UnloadRender, renderer.active->ptr() );
	renderer.unload();
	notifyPlugins( pe_UnloadInput, renderer.active->ptr() );
	input.unload();
	return exitOk;
}

void updateAppPath()
{
	do {
		appPath.resize( appPath.size() == 0 ? 512 : (appPath.size()*2) );
		GetModuleFileName( NULL, &appPath[0], (DWORD)appPath.size() );
	} while( GetLastError() == ERROR_INSUFFICIENT_BUFFER );
	std::wstring::size_type p = appPath.find_last_of( '\\' );
	if( std::wstring::npos == p ) {
		p = appPath.find_last_of( '/' );
		if( std::wstring::npos == p ) {
			return;
		}
	}
	appPath.erase( 1+p );
}

template<class T>
void clearAndDelete( std::vector<T*>& pluginArray )
{
	while( !pluginArray.empty() ) {
		T* p = pluginArray.back();
		pluginArray.pop_back();
		delete p;
	}
}

void appendList( const std::vector<plugin*>& plug )
{
	for( size_t n = 0; n < plug.size(); ++n ){
		const plugin* p = plug[n];
		AppendMenu( hPopupMenu, MF_STRING, (UINT_PTR)p, p->name().c_str() );
	}
}

template<typename T>
bool updateFromCommandline( pluginContext<T>& ctx, const wchar_t* arg )
{
	for( size_t n = 0; n < ctx.all.size(); ++n ){
		plugin* plug = ctx.all[n];
		if( !_wcsicmp(plug->name().c_str(), arg) ) {
			ctx.next = plug;
			return true;
		}
	}
	return false;
}

bool updateAllFromCommandline( const wchar_t* arg )
{
	if( !updateFromCommandline( renderer, arg ) ) {
		return updateFromCommandline( input, arg );
	}
	return true;
}

#define MSG_BUF_SIZE	2048

void appendName( wchar_t* buf, bool checkFirst, const std::vector<plugin*>& all )
{
	for( size_t n = 0; n < all.size(); ++n ){
		const plugin* plug = all[n];
		if( n > 0 || !checkFirst ) {
			wcscat_s( buf, MSG_BUF_SIZE, L", " );
		}
		wcscat_s( buf, MSG_BUF_SIZE, plug->name().c_str() );
	}
}

void init_crt_shit();
void updateLists();		//update queryable list for our plugins.

extern "C" IMAGE_DOS_HEADER __ImageBase;
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int )
{
#ifndef _WIN64
	init_crt_shit();	//see crt_funcs.cpp
#endif

	// Init common controls, only two lines called from one place, I don't think we need a seperate function for it,
	// even though it will probably be inlined during Release optimization. :)
	INITCOMMONCONTROLSEX comEx = { sizeof(INITCOMMONCONTROLSEX), ICC_STANDARD_CLASSES };
	InitCommonControlsEx( &comEx );

	updateAppPath();
	plugin::getPlugins( appPath, renderer.all, plugin::Renderer );
	plugin::getPlugins( appPath, input.all, plugin::Input );
	testPlugin::getPlugins( appPath, notifyPluginList );
	updateLists();
	hPopupMenu = CreatePopupMenu();
	appendList( renderer.all );
	AppendMenu( hPopupMenu, MF_SEPARATOR, 0, 0 );
	appendList( input.all );
	AppendMenu( hPopupMenu, MF_SEPARATOR, 0, 0 );
	AppendMenu( hPopupMenu, MF_STRING, 1, L"Exit" );
	hInstance = reinterpret_cast<HINSTANCE>(&__ImageBase);

	initialMode = baseRender::render2D;

	// Parse command line arguments.
	int argc = 0;
	wchar_t** argv = CommandLineToArgvW( GetCommandLineW(), &argc );
	if( argv ) {
		for( int n = 1; n < argc; ++n ) {
			if( _wcsicmp( argv[n], L"3d" ) == 0) {
				initialMode = baseRender::render3D;
			} else if( _wcsicmp( argv[n], L"2d" ) == 0) {
				initialMode = baseRender::render2D;
			}
			else if (_wcsicmp(argv[n], L"?") == 0 || _wcsicmp(argv[n], L"h") == 0 || _wcsicmp(argv[n], L"help") == 0)
			{
				LocalFree(argv);
				wchar_t* buf = new wchar_t[MSG_BUF_SIZE];
				swprintf_s( buf, MSG_BUF_SIZE, L"Usage: GFXTest [plugin] [mode]\n\n" );
				wcscat_s( buf, MSG_BUF_SIZE, L"Where plugin can be any of: \n  " );
				appendName( buf, true, renderer.all );
				appendName( buf, renderer.all.empty(), input.all );
				wcscat_s( buf, MSG_BUF_SIZE, L"\n\nAnd mode can be any of:\n  2D, 3D" );
				MessageBox( NULL, buf, L"GFXTest info", MB_ICONINFORMATION );
				delete[] buf;
				ExitProcess(0);
			} else {
				updateAllFromCommandline( argv[n] );
			}
		}
		LocalFree( argv );
	}

	if( !renderer.next && !renderer.all.empty() ) {
		renderer.next = renderer.all[0];
	}

	if( !input.next && !input.all.empty() ) {
		input.next = input.all[0];
	}

	// Start application loop. This loop runs the windows and calls the renderer functions.
	while( keepRunning ) {
		renderer.advance();
		input.advance();

		if( !createWindowAndRun( initialMode ) ) {
			if( renderer.active->isFailed() ) {
				if( !renderer.advanceAfterFailure() ) {
					MessageBox( NULL, TEXT("Sorry, all render plugins failed to load, there is nothing more I can do..."), TEXT("GFXTest Error"), MB_ICONERROR );
					keepRunning = false;
				}
			}
			if( input.active->isFailed() ) {
				if( !input.advanceAfterFailure() ) {
					MessageBox( NULL, TEXT("Sorry, all input plugins failed to load, there is nothing more I can do..."), TEXT("GFXTest Error"), MB_ICONERROR );
					keepRunning = false;
				}
			}
		}
	}

	// Finalize the renderer and input plugins and exit the application.
	clearAndDelete( renderer.all );
	clearAndDelete( input.all );
	clearAndDelete( notifyPluginList );
	ExitProcess( 0 );
}