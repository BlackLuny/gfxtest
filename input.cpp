#include "winclude.h"
#include <vector>
#include <algorithm>
#include "gfxtestPlugin.h"

std::vector<wchar_t*> inputData;		// 'threadsafe' because it's only used from our own main thread.
std::vector<float> inputTiming;			// normally would use an std::pair<> with the wchar_t* but we want to use the inputData as wchar_t**
int screenSize[2] = {0};				// only written on init.

bool once = true;
CRITICAL_SECTION queueSection;
std::vector<wchar_t*> queueData;		//used from multiple threads, so protected with a CS
int queuePos[2] = {0};


void notifyPlugins( int pluginEvent, void* arg );		//c-style prototypes everywere :(

void i_init( int sSize[2] )
{
	if( once ) {
		InitializeCriticalSection( &queueSection );
		inputData.push_back( NULL );	//our null delimiter
		inputTiming.push_back( 0.f );	//keep arrays in sync
		once = false;
	}
	for( size_t n = 0; n < 2; ++n ) {
		screenSize[n] = sSize[n];
	}
}

void queuePush( wchar_t* data )
{
	EnterCriticalSection( &queueSection );
	queueData.push_back( data );
	LeaveCriticalSection( &queueSection );
}


void i_mouseMoveTo( int xPosition, int yPosition )
{
	int dum[2] = { xPosition, yPosition };
	notifyPlugins( pe_MouseAbs, dum );
	EnterCriticalSection( &queueSection );
	for( size_t n = 0; n < 2; ++n ) {
		queuePos[n] = std::min( std::max( 0, dum[n] ), screenSize[n] );		//by using dum here instead of xPos, yPos we allow plugins to modify the data :)
	}
	memcpy( queuePos, dum, sizeof(dum) );
	LeaveCriticalSection( &queueSection );
}

void i_mouseMoveAdd( int xAdd, int yAdd )
{
	if( xAdd || yAdd ) {
		int dum[2] = { xAdd, yAdd };
		notifyPlugins( pe_MouseRel, dum );
		EnterCriticalSection( &queueSection );
		for( size_t n = 0; n < 2; ++n ) {
			queuePos[n] = std::min( std::max( 0, queuePos[n] + dum[n] ), screenSize[n] );		//by using dum here instead of xPos, yPos we allow plugins to modify the data :)
		}
		LeaveCriticalSection( &queueSection );
	}
}

const wchar_t* button2str( MouseButtons_e button )
{
	switch( button ) {
		case mb_None: return L"None";
		case mb_Left: return L"Left";
		case mb_Middle: return L"Middle";
		case mb_Right: return L"Right";
		case mb_Button4: return L"Button4";
		case mb_Button5: return L"Button5";
		default: return L"?";
	}
}

void i_mouseButton( MouseButtons_e button, bool down )
{
	int arg = (button | (down ? MOUSEBUTTON_DOWN : 0 ));
	notifyPlugins( pe_MouseButton, &arg );
	button = static_cast<MouseButtons_e>( arg & 0xffff );
	down = (arg & MOUSEBUTTON_DOWN) != 0;
	if( button != mb_None ) {
		static wchar_t buf[100];
		swprintf_s( buf, L"MB: %s: %s", button2str(button), down ? L"down" : L"up" );
		wchar_t* p = _wcsdup( buf );
		queuePush( p );
	}
}

void escapePressed();
void i_keyEvent( long scanCode, bool down )
{
	int arg = (scanCode | (down ? KEY_DOWN : 0 ));
	notifyPlugins( pe_Key, &arg );
	scanCode = static_cast<long>( arg & 0xfffffffe );
	down = (arg & KEY_DOWN) != 0;
	if( scanCode ) {
		static wchar_t buf[100], buf2[100];
		GetKeyNameTextW( scanCode, buf2, _countof(buf2) );
		swprintf_s( buf, L"KB: %s: %s", buf2, down ? L"down" : L"up" );
		wchar_t* p = _wcsdup( buf );
		queuePush( p );
		if( scanCode == (1<<16) && down == false ) {
			escapePressed();
		}
	}
}

wchar_t** i_frame( float tick, int mousePos[2] )
{
	EnterCriticalSection( &queueSection );
	for( size_t n = 0; n < queueData.size(); ++n ) {
		inputData.insert( inputData.end() - 1, queueData[n] );
		inputTiming.insert( inputTiming.end() - 1, tick );
	}
	queueData.clear();
	for( size_t n = 0; n < 2; ++n ) {
		mousePos[n] = queuePos[n];
	}
	LeaveCriticalSection( &queueSection );

	//11, because our last item is a null delimiter, and we want 10 items max.
	// ticks are in ms
	while( ((inputData.size() > 11) || (tick-inputTiming[0] > 6000.f)) && inputData.size() > 1 ) {
		free( inputData[0] );
		inputData.erase( inputData.begin() );
		inputTiming.erase( inputTiming.begin() );
	}
	return inputData.data();
}