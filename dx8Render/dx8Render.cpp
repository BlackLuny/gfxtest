#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include "dx8Render.h"

#include "sdk/d3d8.h"
#include "sdk/d3dx8math.h"

#pragma comment(lib, "d3d8.lib")
#pragma comment(lib, "d3dx8.lib")

struct vertex2d { float x, y, z, rhw; DWORD color; };
#define fvf2D (D3DFVF_XYZRHW | D3DFVF_DIFFUSE)

struct vertex3d { float x, y, z; DWORD color; };
#define fvf3D (D3DFVF_XYZ | D3DFVF_DIFFUSE)

struct LineVertex { D3DXVECTOR3 pos; float rhw; D3DXCOLOR color; };

extern pluginHelper* helper;

extern "C" __declspec(dllexport)
basePlugin* createRender( pluginHelper* help, int version )
{
	if( PLUGINRENDER_VERSION != version ) {
		return NULL;
	}
	helper = help;
	return new dx8Render();
}

extern "C" __declspec(dllexport)
void destroyRender( basePlugin* render )
{
	delete render;
}

struct dx8Info
{
	LPDIRECT3D8 d3d8;
	LPDIRECT3DDEVICE8 d3d8Device;
	LPDIRECT3DVERTEXBUFFER8 d3d8Buffer;
	LPD3DXFONT d3dFont;
	LPDIRECT3DVERTEXBUFFER8 d3d8LineBuffer;
};


const bool dx8Render::GFXTestSwitchModeTo2D()
{
	D3DCOLOR objCol = D3DCOLOR_XRGB(Object.r, Object.g, Object.b);
	vertex2d vertices[] =
	{
		{ Width / 2.f, (float)Offset, 0.5f, 1.0f, objCol, },
		{ (float)(Width - Offset), (float)(Height - Offset), 0.5f, 1.0f, objCol, },
		{ (float)(Offset), (float)(Height - Offset), 0.5f, 1.0f, objCol, },
	};

	info.d3d8Device->CreateVertexBuffer(sizeof(vertices), 0, fvf2D, D3DPOOL_MANAGED, &info.d3d8Buffer);
	void* pVoid;
	info.d3d8Buffer->Lock(0, 0, (BYTE**)&pVoid, 0);
	memcpy(pVoid, vertices, sizeof(vertices));
	info.d3d8Buffer->Unlock();

	return true;
}

const bool dx8Render::GFXTestSwitchModeTo3D()
{
	vertex3d vertices[] =
	{
		// Top Face
		{-5.0f, 5.0f, -5.0f, D3DCOLOR_XRGB(0, 0, 255),},  // Vertex 0 - Blue
		{-5.0f, 5.0f, 5.0f, D3DCOLOR_XRGB(255, 0, 0),},   // Vertex 1 - Red
		{5.0f, 5.0f, -5.0f, D3DCOLOR_XRGB(255, 0, 0),},   // Vertex 2 - Red
		{5.0f, 5.0f, 5.0f, D3DCOLOR_XRGB(0, 255, 0),},    // Vertex 3 - Green

		// Face 1
		{-5.0f, -5.0f, -5.0f, D3DCOLOR_XRGB(255, 0, 0),}, // Vertex 4 - Red
		{-5.0f, 5.0f, -5.0f, D3DCOLOR_XRGB(0, 0, 255),},  // Vertex 5 - Blue
		{5.0f, -5.0f, -5.0f, D3DCOLOR_XRGB(0, 255, 0),},  // Vertex 6 - Green
		{5.0f, 5.0f, -5.0f, D3DCOLOR_XRGB(255, 0, 0),},   // Vertex 7 - Red

		// Face 2
		{5.0f, -5.0f, 5.0f, D3DCOLOR_XRGB(0, 0, 255),},   // Vertex 8 - Blue
		{5.0f, 5.0f, 5.0f, D3DCOLOR_XRGB(0, 255, 0),},    // Vertex 9 - Green

		// Face 3
		{-5.0f, -5.0f, 5.0f, D3DCOLOR_XRGB(0, 255, 0),}, //Vertex 10 - Green
		{-5.0f, 5.0f, 5.0f, D3DCOLOR_XRGB(255, 0, 0),}, //Vertex 11 - Red

		// Face 4
		{-5.0f, -5.0f, -5.0f, D3DCOLOR_XRGB(255, 0, 0),}, //Vertex 12 - Red
		{-5.0f, 5.0f, -5.0f, D3DCOLOR_XRGB(0, 0, 255),}, //Vertex 13 - Blue

		// Bottom Face
		{5.0f, -5.0f, -5.0f, D3DCOLOR_XRGB(0, 255, 0),}, //Vertex 14 - Green
		{5.0f, -5.0f, 5.0f, D3DCOLOR_XRGB(0, 0, 255),}, //Vertex 15 - Blue
		{-5.0f, -5.0f, -5.0f, D3DCOLOR_XRGB(255, 0, 0),}, //Vertex 16 - Red
		{-5.0f, -5.0f, 5.0f, D3DCOLOR_XRGB(0, 255, 0),}, //Vertex 17 - Green
	};

	info.d3d8Device->CreateVertexBuffer(sizeof(vertices), 0, fvf3D, D3DPOOL_MANAGED, &info.d3d8Buffer);
	VOID* pVoid;
	info.d3d8Buffer->Lock(0, 0, (BYTE**)&pVoid, 0);
	memcpy(pVoid, vertices, sizeof(vertices));
	info.d3d8Buffer->Unlock();

	return true;
}

dx8Render::dx8Render() : info(*(new dx8Info()))
{
	memset( &info, 0, sizeof(dx8Info) );
}

dx8Render::~dx8Render()
{
	delete &info;
}

bool dx8Render::init( HWND hWnd )
{
	if (!(info.d3d8 = Direct3DCreate8(D3D_SDK_VERSION)))
	{
		return false;
	}

	D3DDISPLAYMODE d3ddm;
	if (FAILED(info.d3d8->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm)))
	{
		return false;
	}

	D3DPRESENT_PARAMETERS d3dpp = {0};
	d3dpp.Windowed = TRUE;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow = hWnd;
	d3dpp.BackBufferFormat = d3ddm.Format;
	d3dpp.BackBufferWidth = Width;
	d3dpp.BackBufferHeight = Height;

	if (FAILED(info.d3d8->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp, &info.d3d8Device)))
	{
		return false;
	}

	info.d3d8Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	info.d3d8Device->SetRenderState(D3DRS_LIGHTING, FALSE);

	// Create font to draw fps while rendering.
	HFONT fpsFont = CreateFont(16, 0, 0, 0, FW_BOLD, 0, 0, 0, 0, 0, 0, 0, DT_LEFT | DT_TOP, L"Microsoft Sans Serif");
	D3DXCreateFont(info.d3d8Device, fpsFont, &info.d3dFont);
	DeleteObject(fpsFont);

	this->onChangeRenderMode();
	return true;
}

void dx8Render::onChangeRenderMode()
{
	safeRelease(info.d3d8Buffer);

	if (ActiveMode == render2D)
	{
		this->GFXTestSwitchModeTo2D();
	}
	else
	{
		this->GFXTestSwitchModeTo3D();
	}
}

void dx8Render::frameTick( void*, const float tick, float fps, wchar_t** gfxInfo, int mousePos[2] )
{
	info.d3d8Device->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(Background.r, Background.g, Background.b), 1.0f, 0);
	
	info.d3d8Device->BeginScene();

	if (ActiveMode == render2D)
	{
		info.d3d8Device->SetStreamSource(0, info.d3d8Buffer, sizeof(vertex2d));
		info.d3d8Device->SetVertexShader(fvf2D);
		info.d3d8Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);
	}
	else
	{
		//rotation
		D3DXMATRIX matWorld, matWorldX, matWorldY, matWorldZ;
		D3DXMatrixRotationX(&matWorldX, 0);
		D3DXMatrixRotationY(&matWorldY, tick/400.0f);
		D3DXMatrixRotationZ(&matWorldZ, 0);

		D3DXMatrixMultiply(&matWorld, &matWorldX, &matWorldY);
		D3DXMatrixMultiply(&matWorld, &matWorld, &matWorldZ);

		info.d3d8Device->SetTransform( D3DTS_WORLD, &matWorld );
		//camera:
		D3DXMATRIX matView;
		D3DXMatrixLookAtLH(&matView, &D3DXVECTOR3(0.0f, 10.0f,-30.0f), //Cam
			&D3DXVECTOR3(0.0f, 0.0f, 0.0f),  //Look At
			&D3DXVECTOR3(0.0f, 1.0f, 0.0f)); //Up

		info.d3d8Device->SetTransform( D3DTS_VIEW, &matView );

		//perspective
		D3DXMATRIX matProj;
		D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI/4, 1.0f, 1.0f, 500.0f );
		info.d3d8Device->SetTransform(D3DTS_PROJECTION, &matProj);

		//yay, we get to draw finally
		info.d3d8Device->SetStreamSource(0, info.d3d8Buffer, sizeof(vertex3d));
		info.d3d8Device->SetVertexShader(fvf3D);
		info.d3d8Device->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, 2 );  // Top
		info.d3d8Device->DrawPrimitive( D3DPT_TRIANGLESTRIP, 4, 8 );  // Sides
		info.d3d8Device->DrawPrimitive( D3DPT_TRIANGLESTRIP, 14, 2 ); // Bottom
	}

	// Draw FPS on screen
	const wchar_t* buf = helper->va(L"fps: %5.02f", fps);
	RECT rc = {10, 10, 0, 0};
	LPD3DXFONT fnt = info.d3dFont;
	rc.top += fnt->DrawTextW(buf, -1, &rc, DT_NOCLIP, D3DCOLOR_XRGB(255, 255, 255));
	while( gfxInfo && *gfxInfo ) {
		rc.top += fnt->DrawTextW(*gfxInfo, -1, &rc, DT_NOCLIP, D3DCOLOR_XRGB(255, 255, 255));
		++gfxInfo;
	}

	// Create vertex buffer for the crossed lines.
	LineVertex lines[] = 
	{
		{ D3DXVECTOR3(0.0f, (float)mousePos[1], 0.0f), 1.0f, D3DXCOLOR(0, 0, 0, 0xFF) },
		{ D3DXVECTOR3((float)this->Width, (float)mousePos[1], 0.0f), 1.0f, D3DXCOLOR(0, 0, 0, 0xFF) },
		{ D3DXVECTOR3((float)mousePos[0], 0.0f, 0.0f), 1.0f, D3DXCOLOR(0, 0, 0, 0xFF) },
		{ D3DXVECTOR3((float)mousePos[0], (float)this->Height, 0.0f), 1.0f, D3DXCOLOR(0, 0, 0, 0xFF) },
	};

	// Create vertex buffer for the 2D lines.
	info.d3d8Device->CreateVertexBuffer(sizeof(lines) * sizeof(LineVertex), D3DUSAGE_DONOTCLIP | D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY, fvf3D, D3DPOOL_DEFAULT, &info.d3d8LineBuffer);
	VOID* pVoid;
	info.d3d8LineBuffer->Lock(0, 0, (BYTE**)&pVoid, 0);
	memcpy(pVoid, lines, sizeof(lines) * sizeof(LineVertex));
	info.d3d8LineBuffer->Unlock();

	info.d3d8Device->SetStreamSource(0, info.d3d8LineBuffer, sizeof(LineVertex));
    info.d3d8Device->SetVertexShader(fvf2D);
    info.d3d8Device->DrawPrimitive(D3DPT_LINELIST, 0, 2);
	
	info.d3d8LineBuffer->Release();
	info.d3d8LineBuffer = NULL;

	info.d3d8Device->EndScene();
}

void dx8Render::flip( void* )
{
	info.d3d8Device->Present(NULL, NULL, NULL, NULL);
}

void dx8Render::cleanup()
{
	safeRelease(info.d3d8LineBuffer);
	safeRelease(info.d3dFont);
	safeRelease(info.d3d8Buffer);
	safeRelease(info.d3d8Device);
	safeRelease(info.d3d8);
}

// Linker screams for unresolved external...
void main () { }