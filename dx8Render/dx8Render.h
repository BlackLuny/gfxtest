#ifndef DX11RENDER_H
#define DX11RENDER_H

#include "../baseRenderPlugin.h"

struct dx8Info;

class dx8Render : public baseRender
{
public:
	dx8Render();
	virtual ~dx8Render();

	virtual bool isCapable( RenderMode ) const { return true; }
	virtual void onChangeRenderMode();

	virtual bool init( HWND hWnd );
	
	virtual void frameTick( void*, const float tick, float fps, wchar_t** gfxInfo, int mousePos[2] );
	virtual void flip( void* );
	
	virtual void cleanup();
private:
	dx8Render& operator = ( const dx8Render& );
	dx8Info& info;

	__declspec(noinline) const bool GFXTestSwitchModeTo2D();
	__declspec(noinline) const bool GFXTestSwitchModeTo3D();
};

#endif //#ifndef DX9RENDER_H