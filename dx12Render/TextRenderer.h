// Taken from http://www.braynzarsoft.net/viewtutorial/q16390-11-drawing-text-in-directx-12
// Edited structures and LoadFont function by evolution536@unknowncheats

#ifndef __TEXTRENDERER_H__
#define __TEXTRENDERER_H__

#include <string>
#include <d3d12.h>
#include <DirectXMath.h>
#include <Wincodec.h>

using namespace DirectX;

// Represents a text drawing vertex.
struct TextVertex
{
	XMFLOAT4 pos;
	XMFLOAT4 texCoord;
	XMFLOAT4 color;
};

// Represents a character in the text string.
struct FontChar
{
	// the unicode id
	int Id;

	// these need to be converted to texture coordinates 
	// (where 0.0 is 0 and 1.0 is textureWidth of the font)
	float U; // u texture coordinate
	float V; // v texture coordinate
	float TextureWidth; // width of character on texture
	float TextureHeight; // height of character on texture

	float Width; // width of character in screen coords
	float Height; // height of character in screen coords

				  // these need to be normalized based on size of font
	float XOffset; // offset from current cursor pos to left side of character
	float YOffset; // offset from top of line to top of character
	float XAdvance; // how far to move to right for next character

	// Default constructor.
	FontChar()
	{
		this->Id = 0;
		this->U = 0.0f;
		this->V = 0.0f;
		this->TextureWidth = 0.0f;
		this->TextureHeight = 0.0f;
		this->Width = 0.0f;
		this->Height = 0.0f;
		this->XOffset = 0.0f;
		this->YOffset = 0.0f;
		this->XAdvance = 0.0f;
	};
};

// Kerning structure.
struct FontKerning
{
	int FirstId; // the first character
	int SecondId; // the second character
	float Amount; // the amount to add/subtract to second characters x
};

// Unicode string struct definition, for Windows driver prototype structs.
typedef struct _UNICODE_STRING
{
	USHORT Length;
	USHORT MaximumLength;
	PWSTR  Buffer;
} UNICODE_STRING, *PUNICODE_STRING;

struct Font
{
	std::wstring Name; // name of the font
	std::wstring FontImage;
	int Size; // size of font, lineheight and baseheight will be based on this as if this is a single unit (1.0)
	float LineHeight; // how far to move down to next line, will be normalized
	float BaseHeight; // height of all characters, will be normalized
	int TextureWidth; // width of the font texture
	int TextureHeight; // height of the font texture
	int NumCharacters; // number of characters in the font
	FontChar* CharList; // list of characters
	int NumKernings; // the number of kernings
	FontKerning* KerningsList; // list to hold kerning values
	ID3D12Resource* TextureBuffer; // the font texture resource
	bool Bold;
	bool Italic;
	bool Unicode;
	bool Smooth;
	bool IsAntiAliased;
	D3D12_GPU_DESCRIPTOR_HANDLE SrvHandle;
	
	// these are how much the character is padded in the texture. We
	// add padding to give sampling a little space so it does not accidentally
	// padd the surrounding characters. We will need to subtract these paddings
	// from the actual spacing between characters to remove the gaps you would otherwise see
	float LeftPadding;
	float TopPadding;
	float RightPadding;
	float BottomPadding;

	Font() : Name(), FontImage()
	{
		this->Size = 0;
		this->LineHeight = 0.0f;
		this->BaseHeight = 0.0f;
		this->TextureWidth = 0;
		this->TextureHeight = 0;
		this->NumCharacters = 0;
		this->CharList = NULL;
		this->NumKernings = 0;
		this->KerningsList = NULL;
		this->TextureBuffer = NULL;
		this->Bold = false;
		this->Italic = false;
		this->Unicode = false;
		this->Smooth = false;
		this->IsAntiAliased = false;
		this->LeftPadding = 0.0f;
		this->TopPadding = 0.0f;
		this->RightPadding = 0.0f;
		this->BottomPadding = 0.0f;
	};

	~Font()
	{
		if (this->CharList)
		{
			delete[] this->CharList;
			this->CharList = NULL;
		}

		if (this->KerningsList)
		{
			delete[] this->KerningsList;
			this->KerningsList = NULL;
		}

		if (this->TextureBuffer)
		{
			this->TextureBuffer->Release();
			this->TextureBuffer = NULL;
		}
	};

	// this will return the amount of kerning we need to use for two characters
	const float GetKerning(const wchar_t first, const wchar_t second) const
	{
		for (int i = 0; i < NumKernings; ++i)
		{
			if ((wchar_t)KerningsList[i].FirstId == first && (wchar_t)KerningsList[i].SecondId == second)
			{
				return KerningsList[i].Amount;
			}
		}

		return 0.0f;
	};

	// this will return a FontChar given a wide character
	const FontChar* GetChar(const wchar_t c) const
	{
		for (int i = 0; i < NumCharacters; ++i)
		{
			if (c == (wchar_t)CharList[i].Id)
			{
				return &CharList[i];
			}
		}

		return NULL;
	};
};

// This function can be used to load Hiero font files, e.g. the Angel Code font format.
//const bool LoadFont(const wchar_t* const filename, const int windowWidth, const int windowHeight, Font* const outFont);

// Loads and decodes an image file as texture.
//const int LoadImageDataFromFile(BYTE** const imageData, D3D12_RESOURCE_DESC* const resourceDescription, const wchar_t* filename, int* const bytesPerRow);

// Creates buffers for the texture data on the GPU and copies the data to them.
//const bool CreateTextureResources(ID3D12Device* pDevice, ID3D12GraphicsCommandList* pCommandList, Font& refFont, DXGI_FORMAT* const pDxgiFormat);

// Get the DXGI_FORMAT equivilent of a WIC format.
//const DXGI_FORMAT GetDXGIFormatFromWICFormat(const WICPixelFormatGUID& wicFormatGUID);

// Get a DXGI compatible WIC format from another WIC format.
//const WICPixelFormatGUID GetConvertToWICFormat(const WICPixelFormatGUID& wicFormatGUID);

// Get the number of bits per pixel for a DXGI format.
//const int GetDXGIFormatBitsPerPixel(const DXGI_FORMAT& dxgiFormat);

// Creates a root signature specifically for the text drawing vertices.
//const bool CreateTextRendererRootSignature(ID3D12Device* pDevice, ID3D12RootSignature** const ppSignature);

// Creates the required pipeline state object for the text rendering process.
//const bool CreateTextRendererPipelineStateObject(ID3D12Device* pDevice, ID3DBlob* pVertexShader, ID3DBlob* pPixelShader, ID3D12RootSignature* pRootSignature, ID3D12PipelineState** const ppPipelineState);

#endif