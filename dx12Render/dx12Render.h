#ifndef DX12RENDER_H
#define DX12RENDER_H

#include "../baseRenderPlugin.h"
#include <DirectXMath.h>

using namespace DirectX;

struct dx12Info;

class dx12Render : public baseRender
{
public:
	dx12Render();
	virtual ~dx12Render();

	virtual bool isCapable(RenderMode) const { return true; }
	virtual void onChangeRenderMode();

	virtual bool init(HWND hWnd);

	virtual void frameTick(void*, const float tick, float fps, wchar_t** gfxInfo, int mousePos[2]);
	virtual void flip(void*);

	virtual void cleanup();
private:
	dx12Render& operator = (const dx12Render&);
	dx12Info* info;

	__declspec(noinline) const bool GFXTestSwitchModeTo2D();
	__declspec(noinline) const bool GFXTestSwitchModeTo3D();

	void WaitForPreviousFrame();

	// Aligns an address in memory to the specific boundary.
	inline void AlignPointer(DWORD_PTR* const Address, const DWORD Boundary)
	{
		if (Boundary > 0)
		{
			if ((*Address % Boundary) > 0)
			{
				const DWORD_PTR tmp = *Address;
				*Address = (tmp + Boundary) - (tmp % Boundary);
			}
		}
	};

	const bool CreateRootSignature() const;
	//void DrawString(const wchar_t* string, const unsigned int strlen, const XMFLOAT2 position, XMFLOAT2 scale = XMFLOAT2(1.0f, 1.0f), XMFLOAT2 padding = XMFLOAT2(0.0f, 0.0f), XMFLOAT4 color = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f)) const;
};

#endif //#ifndef DX12RENDER_H