#include "TextRenderer.h"
#include "d3dx12.h"
#include "../basePlugin.h"

// This function can be used to load Hiero font files, e.g. the Angel Code font format.
// Expects the outFont pointer parameter to be non-null.
/*const bool LoadFont(const wchar_t* const filename, const int windowWidth, const int windowHeight, Font* const outFont)
{
	// Unless a parse error occurs, the return value should be true.
	bool parseResult = true;

	// Open the font information file.
	HANDLE hFile = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		return false;
	}

	// Get the file size and create a buffer for its contents.
	LARGE_INTEGER fs;
	GetFileSizeEx(hFile, &fs);
	char* fontBuffer = new char[fs.LowPart];

	// Read the file into the buffer.
	DWORD bytesRead;
	if (!ReadFile(hFile, fontBuffer, fs.LowPart, &bytesRead, NULL) || !bytesRead)
	{
		delete[] fontBuffer;
		CloseHandle(hFile);
		return false;
	}

	// Extract the font name.
	const char* fontIterator = strstr(fontBuffer, "info face=\"");
	if (fontIterator)
	{
		fontIterator += 11;
		while (*fontIterator != '\"')
		{
			outFont->Name += *fontIterator++;
		}
	}

	// Get the font size
	fontIterator = strstr(fontIterator, "size=");
	if (fontIterator)
	{
		fontIterator += 5;
		outFont->Size = std::stoi(fontIterator);
	}
	else
	{
		parseResult = false;
	}

	// Check if the font is bold.
	fontIterator = strstr(fontIterator, "bold=");
	if (fontIterator)
	{
		fontIterator += 5;
		outFont->Bold = !!std::stoi(fontIterator);
	}

	// Check if the font is italic.
	fontIterator = strstr(fontIterator, "italic=");
	if (fontIterator)
	{
		fontIterator += 7;
		outFont->Italic = !!std::stoi(fontIterator);
	}

	// Check if the font is Unicode.
	fontIterator = strstr(fontIterator, "unicode=");
	if (fontIterator)
	{
		fontIterator += 8;
		outFont->Unicode = !!std::stoi(fontIterator);
	}

	// Check if the font is smooth.
	fontIterator = strstr(fontIterator, "smooth=");
	if (fontIterator)
	{
		fontIterator += 7;
		outFont->Smooth = !!std::stoi(fontIterator);
	}

	// Check if the font is anti-aliased in the texture.
	fontIterator = strstr(fontIterator, "aa=");
	if (fontIterator)
	{
		fontIterator += 3;
		outFont->IsAntiAliased = !!std::stoi(fontIterator);
	}

	// Examine the font padding fields.
	fontIterator = strstr(fontIterator, "padding=");
	if (fontIterator)
	{
		fontIterator += 8;
		outFont->TopPadding = (float)std::stoi(fontIterator++);
		++fontIterator;
		outFont->RightPadding = (float)std::stoi(fontIterator++);
		++fontIterator;
		outFont->BottomPadding = (float)std::stoi(fontIterator++);
		++fontIterator;
		outFont->LeftPadding = (float)std::stoi(fontIterator);
	}
	else
	{
		parseResult = false;
	}

	// Get the font line height.
	fontIterator = strstr(fontIterator, "lineHeight=");
	if (fontIterator)
	{
		fontIterator += 11;
		outFont->LineHeight = (float)std::stoi(fontIterator) / (float)windowHeight;
	}
	else
	{
		parseResult = false;
	}

	// Get the font base height.
	fontIterator = strstr(fontIterator, "base=");
	if (fontIterator)
	{
		fontIterator += 5;
		outFont->BaseHeight = (float)std::stoi(fontIterator) / (float)windowHeight;
	}
	else
	{
		parseResult = false;
	}

	// Get the texture width of the font.
	fontIterator = strstr(fontIterator, "scaleW=");
	if (fontIterator)
	{
		fontIterator += 7;
		outFont->TextureWidth = std::stoi(fontIterator);
	}
	else
	{
		parseResult = false;
	}

	// Get the texture height of the font.
	fontIterator = strstr(fontIterator, "scaleH=");
	if (fontIterator)
	{
		fontIterator += 7;
		outFont->TextureHeight = std::stoi(fontIterator);
	}
	else
	{
		parseResult = false;
	}

	// Get the texture filename for the font.
	fontIterator = strstr(fontIterator, "file=\"");
	if (fontIterator)
	{
		fontIterator += 6;
		while (*fontIterator != '\"')
		{
			outFont->FontImage += *fontIterator++;
		}
	}
	else
	{
		parseResult = false;
	}

	// Get the number of characters the font contains.
	fontIterator = strstr(fontIterator, "chars count=");
	if (fontIterator)
	{
		fontIterator += 12;
		outFont->NumCharacters = std::stoi(fontIterator);
	}
	else
	{
		parseResult = false;
	}

	// Initialize the font character list.
	outFont->CharList = new FontChar[outFont->NumCharacters];

	// Load all available characters into the character list.
	for (int c = 0; c < outFont->NumCharacters; ++c)
	{
		// Find the next character Unicode id.
		fontIterator = strstr(fontIterator, "char id=");
		if (fontIterator)
		{
			fontIterator += 8;
			outFont->CharList[c].Id = std::stoi(fontIterator);
		}

		// Get the x coordinate of the character in the texture.
		fontIterator = strstr(fontIterator, "x=");
		if (fontIterator)
		{
			fontIterator += 2;
			outFont->CharList[c].U = (float)std::stoi(fontIterator) / (float)outFont->TextureWidth;
		}

		// Get the y coordinate of the character in the texture.
		fontIterator = strstr(fontIterator, "y=");
		if (fontIterator)
		{
			fontIterator += 2;
			outFont->CharList[c].V = (float)std::stoi(fontIterator) / (float)outFont->TextureHeight;
		}

		// Get the width of the character in the texture.
		fontIterator = strstr(fontIterator, "width=");
		if (fontIterator)
		{
			fontIterator += 6;
			const float cWidth = (float)std::stoi(fontIterator);
			outFont->CharList[c].Width = cWidth / (float)windowWidth;
			outFont->CharList[c].TextureWidth = cWidth / (float)outFont->TextureWidth;
		}

		// Get the height of the character in the texture.
		fontIterator = strstr(fontIterator, "height=");
		if (fontIterator)
		{
			fontIterator += 7;
			const float cHeight = (float)std::stoi(fontIterator);
			outFont->CharList[c].Height = cHeight / (float)windowHeight;
			outFont->CharList[c].TextureHeight = cHeight / (float)outFont->TextureHeight;
		}

		// Get the x-offset of the character in the texture.
		fontIterator = strstr(fontIterator, "xoffset=");
		if (fontIterator)
		{
			fontIterator += 8;
			outFont->CharList[c].XOffset = (float)std::stoi(fontIterator) / (float)windowWidth;
		}

		// Get the y-offset of the character in the texture.
		fontIterator = strstr(fontIterator, "yoffset=");
		if (fontIterator)
		{
			fontIterator += 8;
			outFont->CharList[c].YOffset = (float)std::stoi(fontIterator) / (float)windowHeight;
		}

		// Get the xadvance of the character in the texture.
		fontIterator = strstr(fontIterator, "xadvance=");
		if (fontIterator)
		{
			fontIterator += 9;
			outFont->CharList[c].XAdvance = (float)std::stoi(fontIterator) / (float)windowWidth;
		}
	}

	// Get the number of kernings in the font.
	fontIterator = strstr(fontIterator, "kernings count=");
	if (fontIterator)
	{
		fontIterator += 15;
		outFont->NumKernings = std::stoi(fontIterator);
	}

	// Initialize the font kernings list.
	outFont->KerningsList = new FontKerning[outFont->NumKernings];

	for (int k = 0; k < outFont->NumKernings; ++k)
	{
		// Get the first character.
		fontIterator = strstr(fontIterator, "kerning first=");
		if (fontIterator)
		{
			fontIterator += 14;
			outFont->KerningsList[k].FirstId = std::stoi(fontIterator);
		}

		// Get the second character.
		fontIterator = strstr(fontIterator, "second=");
		if (fontIterator)
		{
			fontIterator += 7;
			outFont->KerningsList[k].SecondId = std::stoi(fontIterator);
		}

		// Get the character amount.
		fontIterator = strstr(fontIterator, "amount=");
		if (fontIterator)
		{
			fontIterator += 7;
			outFont->KerningsList[k].Amount = (float)std::stoi(fontIterator) / (float)windowWidth;
		}
	}

	// Clear used resources and return true.
	delete[] fontBuffer;
	CloseHandle(hFile);
	return parseResult;
}

// Get the DXGI_FORMAT equivilent of a WIC format.
const DXGI_FORMAT GetDXGIFormatFromWICFormat(const WICPixelFormatGUID& wicFormatGUID)
{
	if (wicFormatGUID == GUID_WICPixelFormat128bppRGBAFloat) return DXGI_FORMAT_R32G32B32A32_FLOAT;
	else if (wicFormatGUID == GUID_WICPixelFormat64bppRGBAHalf) return DXGI_FORMAT_R16G16B16A16_FLOAT;
	else if (wicFormatGUID == GUID_WICPixelFormat64bppRGBA) return DXGI_FORMAT_R16G16B16A16_UNORM;
	else if (wicFormatGUID == GUID_WICPixelFormat32bppRGBA) return DXGI_FORMAT_R8G8B8A8_UNORM;
	else if (wicFormatGUID == GUID_WICPixelFormat32bppBGRA) return DXGI_FORMAT_B8G8R8A8_UNORM;
	else if (wicFormatGUID == GUID_WICPixelFormat32bppBGR) return DXGI_FORMAT_B8G8R8X8_UNORM;
	else if (wicFormatGUID == GUID_WICPixelFormat32bppRGBA1010102XR) return DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM;
	else if (wicFormatGUID == GUID_WICPixelFormat32bppRGBA1010102) return DXGI_FORMAT_R10G10B10A2_UNORM;
	else if (wicFormatGUID == GUID_WICPixelFormat16bppBGRA5551) return DXGI_FORMAT_B5G5R5A1_UNORM;
	else if (wicFormatGUID == GUID_WICPixelFormat16bppBGR565) return DXGI_FORMAT_B5G6R5_UNORM;
	else if (wicFormatGUID == GUID_WICPixelFormat32bppGrayFloat) return DXGI_FORMAT_R32_FLOAT;
	else if (wicFormatGUID == GUID_WICPixelFormat16bppGrayHalf) return DXGI_FORMAT_R16_FLOAT;
	else if (wicFormatGUID == GUID_WICPixelFormat16bppGray) return DXGI_FORMAT_R16_UNORM;
	else if (wicFormatGUID == GUID_WICPixelFormat8bppGray) return DXGI_FORMAT_R8_UNORM;
	else if (wicFormatGUID == GUID_WICPixelFormat8bppAlpha) return DXGI_FORMAT_A8_UNORM;

	else return DXGI_FORMAT_UNKNOWN;
}

// Get a DXGI compatible WIC format from another WIC format.
const WICPixelFormatGUID GetConvertToWICFormat(const WICPixelFormatGUID& wicFormatGUID)
{
	if (wicFormatGUID == GUID_WICPixelFormatBlackWhite) return GUID_WICPixelFormat8bppGray;
	else if (wicFormatGUID == GUID_WICPixelFormat1bppIndexed) return GUID_WICPixelFormat32bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat2bppIndexed) return GUID_WICPixelFormat32bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat4bppIndexed) return GUID_WICPixelFormat32bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat8bppIndexed) return GUID_WICPixelFormat32bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat2bppGray) return GUID_WICPixelFormat8bppGray;
	else if (wicFormatGUID == GUID_WICPixelFormat4bppGray) return GUID_WICPixelFormat8bppGray;
	else if (wicFormatGUID == GUID_WICPixelFormat16bppGrayFixedPoint) return GUID_WICPixelFormat16bppGrayHalf;
	else if (wicFormatGUID == GUID_WICPixelFormat32bppGrayFixedPoint) return GUID_WICPixelFormat32bppGrayFloat;
	else if (wicFormatGUID == GUID_WICPixelFormat16bppBGR555) return GUID_WICPixelFormat16bppBGRA5551;
	else if (wicFormatGUID == GUID_WICPixelFormat32bppBGR101010) return GUID_WICPixelFormat32bppRGBA1010102;
	else if (wicFormatGUID == GUID_WICPixelFormat24bppBGR) return GUID_WICPixelFormat32bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat24bppRGB) return GUID_WICPixelFormat32bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat32bppPBGRA) return GUID_WICPixelFormat32bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat32bppPRGBA) return GUID_WICPixelFormat32bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat48bppRGB) return GUID_WICPixelFormat64bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat48bppBGR) return GUID_WICPixelFormat64bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat64bppBGRA) return GUID_WICPixelFormat64bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat64bppPRGBA) return GUID_WICPixelFormat64bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat64bppPBGRA) return GUID_WICPixelFormat64bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat48bppRGBFixedPoint) return GUID_WICPixelFormat64bppRGBAHalf;
	else if (wicFormatGUID == GUID_WICPixelFormat48bppBGRFixedPoint) return GUID_WICPixelFormat64bppRGBAHalf;
	else if (wicFormatGUID == GUID_WICPixelFormat64bppRGBAFixedPoint) return GUID_WICPixelFormat64bppRGBAHalf;
	else if (wicFormatGUID == GUID_WICPixelFormat64bppBGRAFixedPoint) return GUID_WICPixelFormat64bppRGBAHalf;
	else if (wicFormatGUID == GUID_WICPixelFormat64bppRGBFixedPoint) return GUID_WICPixelFormat64bppRGBAHalf;
	else if (wicFormatGUID == GUID_WICPixelFormat64bppRGBHalf) return GUID_WICPixelFormat64bppRGBAHalf;
	else if (wicFormatGUID == GUID_WICPixelFormat48bppRGBHalf) return GUID_WICPixelFormat64bppRGBAHalf;
	else if (wicFormatGUID == GUID_WICPixelFormat128bppPRGBAFloat) return GUID_WICPixelFormat128bppRGBAFloat;
	else if (wicFormatGUID == GUID_WICPixelFormat128bppRGBFloat) return GUID_WICPixelFormat128bppRGBAFloat;
	else if (wicFormatGUID == GUID_WICPixelFormat128bppRGBAFixedPoint) return GUID_WICPixelFormat128bppRGBAFloat;
	else if (wicFormatGUID == GUID_WICPixelFormat128bppRGBFixedPoint) return GUID_WICPixelFormat128bppRGBAFloat;
	else if (wicFormatGUID == GUID_WICPixelFormat32bppRGBE) return GUID_WICPixelFormat128bppRGBAFloat;
	else if (wicFormatGUID == GUID_WICPixelFormat32bppCMYK) return GUID_WICPixelFormat32bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat64bppCMYK) return GUID_WICPixelFormat64bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat40bppCMYKAlpha) return GUID_WICPixelFormat64bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat80bppCMYKAlpha) return GUID_WICPixelFormat64bppRGBA;

#if (_WIN32_WINNT >= _WIN32_WINNT_WIN8) || defined(_WIN7_PLATFORM_UPDATE)
	else if (wicFormatGUID == GUID_WICPixelFormat32bppRGB) return GUID_WICPixelFormat32bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat64bppRGB) return GUID_WICPixelFormat64bppRGBA;
	else if (wicFormatGUID == GUID_WICPixelFormat64bppPRGBAHalf) return GUID_WICPixelFormat64bppRGBAHalf;
#endif

	else return GUID_WICPixelFormatDontCare;
}

// Get the number of bits per pixel for a DXGI format.
const int GetDXGIFormatBitsPerPixel(const DXGI_FORMAT& dxgiFormat)
{
	if (dxgiFormat == DXGI_FORMAT_R32G32B32A32_FLOAT) return 128;
	else if (dxgiFormat == DXGI_FORMAT_R16G16B16A16_FLOAT) return 64;
	else if (dxgiFormat == DXGI_FORMAT_R16G16B16A16_UNORM) return 64;
	else if (dxgiFormat == DXGI_FORMAT_R8G8B8A8_UNORM) return 32;
	else if (dxgiFormat == DXGI_FORMAT_B8G8R8A8_UNORM) return 32;
	else if (dxgiFormat == DXGI_FORMAT_B8G8R8X8_UNORM) return 32;
	else if (dxgiFormat == DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM) return 32;

	else if (dxgiFormat == DXGI_FORMAT_R10G10B10A2_UNORM) return 32;
	else if (dxgiFormat == DXGI_FORMAT_B5G5R5A1_UNORM) return 16;
	else if (dxgiFormat == DXGI_FORMAT_B5G6R5_UNORM) return 16;
	else if (dxgiFormat == DXGI_FORMAT_R32_FLOAT) return 32;
	else if (dxgiFormat == DXGI_FORMAT_R16_FLOAT) return 16;
	else if (dxgiFormat == DXGI_FORMAT_R16_UNORM) return 16;
	else if (dxgiFormat == DXGI_FORMAT_R8_UNORM) return 8;
	else if (dxgiFormat == DXGI_FORMAT_A8_UNORM) return 8;

	return DXGI_FORMAT_UNKNOWN;
}

// Loads and decodes an image file as texture.
const int LoadImageDataFromFile(BYTE** const imageData, D3D12_RESOURCE_DESC* const resourceDescription, const wchar_t* filename, int* const bytesPerRow)
{
	// We only need one instance of the imaging factory to create decoders and frames.
	static IWICImagingFactory *wicFactory = NULL;

	// Reset decoder, frame and converter since these will be different for each image we load.
	IWICBitmapDecoder *wicDecoder = NULL;
	IWICBitmapFrameDecode *wicFrame = NULL;
	IWICFormatConverter *wicConverter = NULL;

	bool imageConverted = false;
	if (!wicFactory)
	{
		// Initialize the COM library.
		CoInitialize(NULL);

		// Create the WIC factory.
		if (FAILED(CoCreateInstance(CLSID_WICImagingFactory, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&wicFactory))))
		{
			return 0;
		}
	}

	// Load a decoder for the image.
	if (FAILED(wicFactory->CreateDecoderFromFilename(filename, NULL, GENERIC_READ, WICDecodeMetadataCacheOnLoad, &wicDecoder)))
	{
		return 0;
	}

	// Get image from decoder (this will decode the "frame").
	if (FAILED(wicDecoder->GetFrame(0, &wicFrame)))
	{
		return 0;
	}

	// Get the WIC pixel format of the image.
	WICPixelFormatGUID pixelFormat;
	if (FAILED(wicFrame->GetPixelFormat(&pixelFormat)))
	{
		return 0;
	}

	// Get the size of the image.
	UINT textureWidth, textureHeight;
	if (FAILED(wicFrame->GetSize(&textureWidth, &textureHeight)))
	{
		return 0;
	}

	// Convert WIC pixel format to DXGI pixel format.
	DXGI_FORMAT dxgiFormat = GetDXGIFormatFromWICFormat(pixelFormat);

	// If the format of the image is not a supported DXGI format, try to convert it.
	if (dxgiFormat == DXGI_FORMAT_UNKNOWN)
	{
		// Get a DXGI compatible WIC format from the current image format.
		const WICPixelFormatGUID convertToPixelFormat = GetConvertToWICFormat(pixelFormat);

		// Return if no DXGI compatible format was found.
		if (convertToPixelFormat == GUID_WICPixelFormatDontCare)
		{
			return 0;
		}

		// Set the DXGI format.
		dxgiFormat = GetDXGIFormatFromWICFormat(convertToPixelFormat);

		// Create the format converter.
		if (FAILED(wicFactory->CreateFormatConverter(&wicConverter)))
		{
			return 0;
		}

		// Make sure we can convert to the DXGI compatible format.
		BOOL canConvert = FALSE;
		if (FAILED(wicConverter->CanConvert(pixelFormat, convertToPixelFormat, &canConvert)) || !canConvert)
		{
			return 0;
		}

		// Do the conversion (wicConverter will contain the converted image).
		if (FAILED(wicConverter->Initialize(wicFrame, convertToPixelFormat, WICBitmapDitherTypeErrorDiffusion, 0, 0, WICBitmapPaletteTypeCustom)))
		{
			return 0;
		}

		// This is so we know to get the image data from the wicConverter (otherwise we will get from wicFrame).
		imageConverted = true;
	}

	const int bitsPerPixel = GetDXGIFormatBitsPerPixel(dxgiFormat);
	*bytesPerRow = (textureWidth * bitsPerPixel) / 8;
	const int imageSize = *bytesPerRow * textureHeight;

	// Allocate enough memory for the raw image data, and set imageData to point to that memory.
	*imageData = new BYTE[imageSize];

	// Copy (decoded) raw image data into the newly allocated memory (imageData).
	if (imageConverted)
	{
		// If image format needed to be converted, the wic converter will contain the converted image.
		if (FAILED(wicConverter->CopyPixels(0, *bytesPerRow, imageSize, *imageData)))
		{
			return 0;
		}
	}
	else
	{
		// No need to convert, just copy data from the WIC frame.
		if (FAILED(wicFrame->CopyPixels(0, *bytesPerRow, imageSize, *imageData)))
		{
			return 0;
		}
	}

	// Now describe the texture with the information we have obtained from the image.
	memset(resourceDescription, 0, sizeof(D3D12_RESOURCE_DESC));
	resourceDescription->Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resourceDescription->Width = textureWidth;
	resourceDescription->Height = textureHeight;
	resourceDescription->DepthOrArraySize = 1;
	resourceDescription->MipLevels = 1;
	resourceDescription->Format = dxgiFormat;
	resourceDescription->SampleDesc.Count = 1;

	// Return the size of the image. remember to delete the image once your done with it (in this tutorial once its uploaded to the gpu).
	return imageSize;
}

// Creates buffers for the texture data on the GPU and copies the data to them.
const bool CreateTextureResources(ID3D12Device* pDevice, ID3D12GraphicsCommandList* pCommandList, Font& refFont, DXGI_FORMAT* const pDxgiFormat)
{
	D3D12_RESOURCE_DESC fontTextureDesc;
	int fontImageBytesPerRow;
	BYTE* textureMemory;

	// Load the font texture from the file that is specified in the font information.
	const int fontImageSize = LoadImageDataFromFile(&textureMemory, &fontTextureDesc, refFont.FontImage.c_str(), &fontImageBytesPerRow);

	// Make sure we have the texture data.
	if (!textureMemory || !fontImageSize)
	{
		return false;
	}

	// Export the texture DXGI format to the output pointer.
	*pDxgiFormat = fontTextureDesc.Format;

	// Create the font texture resource.
	if (FAILED(pDevice->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT), D3D12_HEAP_FLAG_NONE, &fontTextureDesc, D3D12_RESOURCE_STATE_COPY_DEST, NULL, IID_PPV_ARGS(&refFont.TextureBuffer))))
	{
		return false;
	}

	ID3D12Resource* fontTextureBufferUploadHeap;
	UINT64 fontTextureUploadBufferSize;
	pDevice->GetCopyableFootprints(&fontTextureDesc, 0, 1, 0, NULL, NULL, NULL, &fontTextureUploadBufferSize);

	// Create an upload heap to copy the texture to the gpu.
	if (FAILED(pDevice->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD), D3D12_HEAP_FLAG_NONE, &CD3DX12_RESOURCE_DESC::Buffer(fontTextureUploadBufferSize), D3D12_RESOURCE_STATE_GENERIC_READ, NULL, IID_PPV_ARGS(&fontTextureBufferUploadHeap))))
	{
		return false;
	}

	// Store font image in upload heap.
	D3D12_SUBRESOURCE_DATA fontTextureData;
	fontTextureData.pData = textureMemory;
	fontTextureData.RowPitch = fontImageBytesPerRow;
	fontTextureData.SlicePitch = fontImageBytesPerRow * fontTextureDesc.Height;

	// Now we copy the upload buffer contents to the default heap.
	UpdateSubresources(pCommandList, refFont.TextureBuffer, fontTextureBufferUploadHeap, 0, 0, 1, &fontTextureData);
	fontTextureBufferUploadHeap->Release();

	// Create a resource barrier for the shader resource view.
	pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(refFont.TextureBuffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));

	delete[] textureMemory;
	return true;
}

// Creates a root signature specifically for the text drawing vertices.
const bool CreateTextRendererRootSignature(ID3D12Device* pDevice, ID3D12RootSignature** const ppSignature)
{
	// Create a descriptor range (descriptor table) and fill it out.
	// This is a range of descriptors inside a descriptor heap.
	CD3DX12_DESCRIPTOR_RANGE descriptorTableRange { D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0 };

	// Create a root parameter for the root descriptor and fill it out.
	CD3DX12_ROOT_PARAMETER rootParameter;
	rootParameter.InitAsDescriptorTable(1, &descriptorTableRange, D3D12_SHADER_VISIBILITY_PIXEL);

	// Create a static sampler.
	D3D12_STATIC_SAMPLER_DESC sampler;
	memset(&sampler, 0, sizeof(sampler));
	sampler.AddressU = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
	sampler.AddressV = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
	sampler.AddressW = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
	sampler.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
	sampler.MaxLOD = D3D12_FLOAT32_MAX;
	sampler.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	// Create the root signature descriptor.
	CD3DX12_ROOT_SIGNATURE_DESC rootSignatureDesc;
	rootSignatureDesc.Init(1, &rootParameter, 1, &sampler,
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS);

	ID3DBlob* errorBuff = NULL;
	ID3DBlob* signature = NULL;

	// Deserialize the root signature data into a blob.
	if (FAILED(D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, &errorBuff)))
	{
		safeRelease(errorBuff);
		return false;
	}

	// Create the root signature.
	if (FAILED(pDevice->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(ppSignature))))
	{
		safeRelease(errorBuff);
		safeRelease(signature);
		return false;
	}

	// The creation has succeeded.
	safeRelease(signature);
	safeRelease(errorBuff);
	return true;
}

// Creates the required pipeline state object for the text rendering process.
const bool CreateTextRendererPipelineStateObject(ID3D12Device* pDevice, ID3DBlob* pVertexShader, ID3DBlob* pPixelShader, ID3D12RootSignature* pRootSignature, ID3D12PipelineState** const ppPipelineState)
{
	// Input layout for the text drawing vertex shader.
	D3D12_INPUT_ELEMENT_DESC textInputLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 16, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 32, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 }
	};

	// Create a FPS text drawing pipeline state object.
	D3D12_GRAPHICS_PIPELINE_STATE_DESC textPsoDesc;
	memset(&textPsoDesc, 0, sizeof(textPsoDesc));

	textPsoDesc.InputLayout = { textInputLayout, _countof(textInputLayout) };
	textPsoDesc.pRootSignature = pRootSignature;
	textPsoDesc.VS.pShaderBytecode = pVertexShader->GetBufferPointer();
	textPsoDesc.VS.BytecodeLength = pVertexShader->GetBufferSize();
	textPsoDesc.PS.pShaderBytecode = pPixelShader->GetBufferPointer();
	textPsoDesc.PS.BytecodeLength = pPixelShader->GetBufferSize();
	textPsoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	textPsoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	textPsoDesc.SampleDesc.Count = 1;
	textPsoDesc.SampleMask = UINT_MAX;
	textPsoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	textPsoDesc.NumRenderTargets = 1;
	textPsoDesc.BlendState.RenderTarget[0].BlendEnable = TRUE;
	textPsoDesc.BlendState.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	textPsoDesc.BlendState.RenderTarget[0].DestBlend = D3D12_BLEND_ONE;
	textPsoDesc.BlendState.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	textPsoDesc.BlendState.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
	textPsoDesc.BlendState.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ONE;
	textPsoDesc.BlendState.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	textPsoDesc.BlendState.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;


	// Create the graphics pipeline for drawing the lines. The only thing that has changed in the descriptor object is the vertex shader and the primitive topology.
	if (FAILED(pDevice->CreateGraphicsPipelineState(&textPsoDesc, IID_PPV_ARGS(ppPipelineState))))
	{
		return false;
	}

	return true;
}*/