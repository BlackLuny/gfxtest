#include "winclude.h"
#include "plugin.h"
#include <string>
#include "baseInputPlugin.h"


void i_mouseMoveTo( int xPosition, int yPosition );
void i_mouseMoveAdd( int xAdd, int yAdd );
void i_mouseButton( MouseButtons_e button, bool down );
void i_keyEvent( long scanCode, bool down );


class pluginHelperImp : public baseInputHelper
{
public:
	virtual ~pluginHelperImp() {;}

	virtual void* alloc( size_t size )
	{
		return ::malloc( size );
	}
	virtual void free( void* p )
	{
		return ::free( p );
	}
	virtual void *memset( void *dest, int c, size_t count )
	{
		return ::memset( dest, c, count );
	}
	virtual const wchar_t* va( const wchar_t* fmt, ... )
	{
		static wchar_t buf[2][1024];
		static int idx = 0;		//this way we can _wcsicmp( va(xxx), va(yyy) )
		idx ^= 1;
		va_list args;
		va_start( args, fmt );
		vswprintf_s( buf[idx], _countof(buf[idx]), fmt, args );
		va_end( args );
		return buf[idx];
	}

	virtual void mouseMoveTo( int xPosition, int yPosition )
	{
		i_mouseMoveTo( xPosition, yPosition );
	}

	virtual void mouseMoveAdd( int xAdd, int yAdd )
	{
		i_mouseMoveAdd( xAdd, yAdd );
	}

	virtual void mouseButton( int button, bool down )
	{
		i_mouseButton( static_cast<MouseButtons_e>(button), down );
	}

	virtual void keyEvent( long scanCode, bool down )
	{
		i_keyEvent( scanCode, down );
	}
};

pluginHelperImp helper;

const wchar_t* typeString( pluginBase::Type type )
{
	switch( type ) {
		case pluginBase::Input: return L"Input";
		case pluginBase::Renderer: return L"Render";
		case pluginBase::Test: return L"TestPlugin";
		default: return L"";
	}
}

const char* vaA( const char* fmt, ... )
{
	static char buf[128];
	va_list args;
	va_start( args, fmt );
	vsprintf_s( buf, _countof(buf), fmt, args );
	va_end( args );
	return buf;
}

std::wstring stripExtension( const std::wstring& name )
{
	std::wstring::size_type ext = name.find_last_of( '.' );
	if( std::wstring::npos != ext ) {
		return name.substr( 0, ext );
	}
	return name;
}


pluginBase::pluginBase(const std::wstring& name, const std::wstring& path, Type type)
: mName(name), mFile(path), mType(type), mModule(NULL)
{
}

pluginBase::~pluginBase()
{
	unload();
}

bool pluginBase::load()
{
	mModule = LoadLibraryW( mFile.c_str() );
	return mModule != NULL;
}

void pluginBase::unload()
{
	if( mModule ) {
		FreeLibrary( mModule );
		mModule = NULL;
	}
}

template<class T>
void pluginBase::buildPluginList( const std::wstring& appPath, std::vector<T*>& plugins, Type type, T* (*loadProc)(const std::wstring&,const std::wstring&,Type) )
{
	while( !plugins.empty() ) {
		delete plugins[0];
		plugins.erase( plugins.begin() );
	}
	std::wstring path = appPath;
	path.append( helper.va( L"%s\\*" PLUGIN_ARCH L".dll", typeString(type) ) );
	WIN32_FIND_DATA wfd = {0};
	HANDLE hFind = FindFirstFile( path.c_str(), &wfd );
	path = appPath;
	path.append( helper.va( L"%s\\", typeString(type) ) );
	if( hFind != INVALID_HANDLE_VALUE ) {
		do {
			std::wstring filePath = path;
			filePath.append( wfd.cFileName );
			T* help = loadProc( filePath, wfd.cFileName, type );
			if( help ) {
				plugins.push_back( help );
			}
		} while( FindNextFile( hFind, &wfd ) );
		FindClose( hFind );
	}
}


plugin::plugin( const std::wstring& name, const std::wstring& path, pluginBase::Type type )
	:pluginBase( name, path, type ), mFailed( false )
{
}

basePlugin* plugin::load( int version )
{
	if( pluginBase::load() ) {
		createProc proc = (createProc)GetProcAddress( mModule, vaA("create%S", typeString(mType) ) );
		if( proc ) {
			basePlugin* ptr = proc(&helper, version);
			if( ptr ) {
				if( ptr->ptr_size() != sizeof(void*) ) {
					callDestroy( ptr );
					ptr = NULL;
				}
				return ptr;
			}
		}
		return NULL;
	}
	return NULL;
}

void plugin::unload( basePlugin* item )
{
	if( mModule ) {
		callDestroy( item );
		pluginBase::unload();
	}
}

void plugin::callDestroy( basePlugin* item )
{
	if( item ) {
		destroyProc proc = (destroyProc)GetProcAddress( mModule, vaA("destroy%S", typeString(mType) ) );
		if( proc ) {
			proc( item );
		}
	}
}

void plugin::getPlugins( const std::wstring& appPath, std::vector<plugin*>& plugins, Type type )
{
	buildPluginList( appPath, plugins, type, plugin::createPluginProc );
}

plugin* plugin::createPluginProc( const std::wstring& fullPath, const std::wstring& fileName, Type type )
{
	HMODULE hMod = LoadLibraryEx( fullPath.c_str(), NULL, LOAD_LIBRARY_AS_DATAFILE );
	plugin* p = NULL;
	if( hMod ) {
		TCHAR* pr = NULL;
		std::wstring name = stripExtension(fileName);
		if( LoadString( hMod, 101, (LPWSTR)&pr, 0 ) ) {
			name = pr;
		}
		p = new plugin( name, fullPath, type );
		FreeLibrary( hMod );
	}
	return p;
}


testPlugin::testPlugin( const std::wstring& name, const std::wstring& path, HMODULE hMod )
: pluginBase( name, path, pluginBase::Test ), mCallback(NULL)
{
	mModule = hMod;
	mCallback = (pluginCallbackProc)GetProcAddress( mModule, PLUGIN_PROCNAME );
	if( !mCallback ) {
		mCallback = (pluginCallbackProc)GetProcAddress( mModule, PLUGIN_ALTPROCNAME );
	}
}


bool testPlugin::load()
{
	return true;
}

void testPlugin::unload()
{
}

void testPlugin::callback( int pluginEvent, void* arg )
{
	if( mCallback ) {
		mCallback( pluginEvent, arg );
	}
}

void testPlugin::getPlugins( const std::wstring& appPath, std::vector<testPlugin*>& plugins )
{
	buildPluginList( appPath, plugins, pluginBase::Test, testPlugin::createTestPluginProc );
}


testPlugin* testPlugin::createTestPluginProc( const std::wstring& fullPath, const std::wstring& fileName, Type )
{
	HMODULE hMod = LoadLibraryEx( fullPath.c_str(), NULL, 0 );
	testPlugin* p = NULL;
	if( hMod ) {
		TCHAR* pr = NULL;
		std::wstring name = stripExtension(fileName);
		if( LoadString( hMod, 101, (LPWSTR)&pr, 0 ) ) {
			name = pr;
		}
		p = new testPlugin( name, fullPath, hMod );
	}
	return p;
}



